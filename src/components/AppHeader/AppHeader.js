import React from 'react';
import {
  Body,
  Header,
  Left,
  Right,
  Title
} from 'native-base';
import { TouchableOpacity, Image, View } from 'react-native';
import PropTypes from 'prop-types'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

class AppHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { onPress, headerTitle, userimg, userprofile, notification, headerTitle1,
      profile, backgo, image1, drawer, date,tintColor,color } = this.props;
    return (

      <Header androidStatusBarColor="#5dca67"
        style={{ backgroundColor: '#fff', borderBottomWidth: 0, shadowOffset: {height: 0, width: 0}, 
        shadowOpacity: 0, elevation: 0 }}>
        <Left style={{ flex: 1,flexDirection:'row' }}>
          {backgo !== undefined &&
            <TouchableOpacity onPress={onPress} >
              <Image source={backgo}
                style={{
                  width: 25,
                  height: 25,
                  marginLeft:1,
                  tintColor: '#391FBB',
                }}>
              </Image>
            </TouchableOpacity>

          }
          {image1 !== undefined &&
            <TouchableOpacity onPress={drawer} >
              <Image source={image1}
                style={{
                  width: 20,
                  height: 25,
                  marginLeft: 10, 
                  tintColor: tintColor

                }}>
              </Image>
            </TouchableOpacity>}
        </Left>
        <View style={{
            flex: 3,
            justifyContent: 'center',
            alignContent: 'center'
          }}>
            {/* <Title style={{
              color: '#391EBB',
             alignSelf:"flex-start",
            }}>
              {headerTitle1}
            </Title> */}
            <Title style={{
              color: color,
              textAlign: "center",
              fontSize: RFValue(15),
              fontWeight:'bold'
            }}>
              {headerTitle}
            </Title>
            {date != undefined &&
              <Title style={{
                color: '#fff',
                textAlign: "center",
                fontSize: 12
              }}>
                {date}
              </Title>
            }

          </View>
          <Right style={{ flexDirection: 'row', flex: 1, }}>
            <TouchableOpacity onPress={notification} >
              <Image source={userimg}
                style={{
                  width: 16,
                  height: 16,
                  marginRight: 20,
                  // tintColor: '#fff'
                }}>
              </Image>
            </TouchableOpacity>
            <TouchableOpacity onPress={profile} >
              <Image source={userprofile}
                style={{ width: 20, height: 20, marginRight: 10, }}>
              </Image>
            </TouchableOpacity>

          </Right>

      </Header>

        );
      }
    }
    
AppHeader.propTypes = {
          onPress: PropTypes.func,
        icon: PropTypes.string,
        onPressRight: PropTypes.func,
        iconRight: PropTypes.string,
        title: PropTypes.string.isRequired
      };
      
export default AppHeader;