import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        opacity: 0.85,
        backgroundColor: 'transparent',
        width: '100%',
        height: '100%',
        zIndex: 999,
        alignSelf: 'center',
        justifyContent: 'center',
        color: '#391EBB'
    }
});

export default styles;