import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, ScrollView, FlatList, PermissionsAndroid, BackHandler
} from "react-native";
import Spinner from '../../components/Loader';
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Contacts from 'react-native-contacts';
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import AsyncStorage from '@react-native-community/async-storage';

class RequestMoneyList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            contacts: null, searching: '',
            Contact_name: '',
            returnAllContacts: [],
            isLoading: false,
        };
        this.searchContact = this.searchContact.bind(this);
    };
    //--------------- ---------get Contact-------------------//
    async componentDidMount() {
        const user_id = await AsyncStorage.getItem('user_id')
        const Authoriz = await AsyncStorage.getItem('Acess_token')
        console.log(user_id, "user_id")
        this.setState({
            user_id: user_id
        })
        isLoading = true
        let contactList = []
        if (Platform.OS === 'ios') {
            Contacts.getAll((err, contacts) => {
                if (err) {
                    throw err;
                }
                // contacts returned
                this.setState({ contacts })
            })
        } else if (Platform.OS === 'android') {
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                    title: 'Contacts',
                    message: 'This app would like to view your contacts.'
                }
            ).then(() => {
                Contacts.getAll((err, contacts) => {
                    if (err === 'denied') {
                        // error
                    } else {
                        // contacts returned in Array
                        for (let item of contacts) {
                            if (item.phoneNumbers.length != 0) {
                                contactList.push({ "user_name": item.displayName, "mobile_number": item.phoneNumbers[0].number, "email": "a@gmail.com" })
                            }
                        }
                        this.setState({ contacts })
                    }
                })
            })
        }
        setTimeout(() => {
            this.inviteButton(contactList,Authoriz)
        }, 3000);
    }
    //--------------- --------- invite button APi-------------------//
    inviteButton = (contactList,Authoriz) => {
        this.setState({ isLoading: true })
        const phoneContacts = {
            "user_id": this.state.user_id,
            "contact": contactList
        }
        fetch('http://jokingfriend.com/Dero/index.php/Api/phoneContacts', {

            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authoriz":Authoriz
            },
            body: JSON.stringify(phoneContacts)
        })
            .then((res) => res.json())
            .then(res => {
                console.log(res)

                if (res.result === true || res.result === "true") {
                    this.setState({
                        isLoading: false,
                        returnAllContacts: res.data,
                    }, () => {
                        console.log(this.state.returnAllContacts)
                    })
                }
                else {
                    alert(res.msg)
                    this.setState({
                        isLoading: false
                    })
                }
            })
            .catch((e) => {
                console.log('Oops')
                console.warn(e);
            });
    };
    //--------------- ---------ASearch_COntact-------------------//
    searchContact(contacts) {
        console.log(contacts)
        this.setState({
            searching: contacts
        })
    }
    render() {
        let filterContactData = [];
        if (filterContactData) {
            filterContactData = this.state.searching ? this.state.returnAllContacts.filter(row => row.name.toLowerCase().indexOf(this.state.searching.toLowerCase()) > -1) : this.state.returnAllContacts;
        }
        return (
            <View style={styles.Container}>
                <AppHeader
                    headerTitle={'Request Money'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                    color={"#636363"}
                />
                <View style={{ marginTop: 10, paddingHorizontal: 20 }}>
                    {
                        (this.state.contacts !== 0) &&
                        <View style={styles.Search_view}>
                            <View>
                                <Image style={styles.search_img}
                                    source={require("../../components/icon/search1.png")} />
                            </View>
                            <View style={{ width: width - 70 }}>
                                <TextInput style={{ marginLeft: 10 }}
                                    placeholder='Search name or number'
                                    placeholderTextColor='grey'
                                    onChangeText={(text) => this.searchContact(text)}>
                                </TextInput>
                            </View>
                        </View>
                    }
                </View>
                <View style={styles.MainView}>
                    <View style={{ marginTop: 20, marginLeft: 30, }}>
                        <Text style={styles.Result_Text}>Results</Text>
                        <View style={styles.Line_View} />
                    </View>
                    <View style={{ marginTop: 20 }}>
                        <ScrollView>
                            {
                                filterContactData && filterContactData.map(item => (
                                    <View style={styles.List_mainView} >
                                        <TouchableOpacity onPress={() => Actions.RequestMoney({ Contact: item })}>
                                            <View style={{ flexDirection: 'row', padding: 10 }}>
                                                <View>
                                                    <Image style={{ height: 47, width: 51 }}
                                                        source={require("../../components/icon/req5.png")} />
                                                </View>
                                                <View style={{ marginLeft: 10, justifyContent: 'center' }}>
                                                    <Text style={styles.name_text}>{item.name}</Text>
                                                    <Text style={styles.contact_text}>{item.contact}</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        {item.status == 0 &&
                                            <TouchableOpacity style={{ justifyContent: 'center', }} >
                                                <LinearGradient
                                                    start={{ x: 0, y: 0 }}
                                                    end={{ x: 1, y: 0 }}
                                                    colors={['#3E1EDD', '#5C48C5']}
                                                    style={styles.gardient}>
                                                    <Text style={styles.btn_text}>Invite</Text>
                                                </LinearGradient>
                                            </TouchableOpacity>
                                        }
                                    </View>
                                ))}
                            <View style={{ height: 100 }} />
                        </ScrollView>
                    </View>
                </View>
                {this.state.isLoading &&
                    <Spinner />
                }
            </View>
        );
    }
}
export default RequestMoneyList;
