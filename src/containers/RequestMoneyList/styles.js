import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    Search_view: {
        flexDirection: 'row',
        borderColor: '#000',
        borderBottomWidth: 2,
        borderBottomColor: '#3F20DD80',
        height: 40,
        alignItems: 'center',
    },
    search_img: {
        height: 15, width: 15, marginLeft: 5, tintColor: '#3F20DD'
    },
    MainView: {
        backgroundColor: '#fff',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        marginTop: 20,
        height: height,
        flex: 1,
    },
    Result_Text: {
        fontSize: RFValue(15),
        color: '#3F20DD',
        fontWeight: 'bold'
    },
    Line_View: {
        height: 2,
        width: '14%',
        backgroundColor: '#3F20DD',
        marginTop: 5
    },

    List_mainView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15
    },
    name_text: {
        color: '#000',
        fontSize: RFValue(18)
    },
    contact_text: {
        color: '#5E5E5E',
        fontSize: RFValue(15)
    },
    gardient: {
        justifyContent: 'center',
        width: 70, height: 30,
        alignItems: 'center',
        backgroundColor: "#391EBB",
        borderRadius: 15
    },
    btn_text: {
        fontSize: RFValue(20),
        color: '#fff',
        fontWeight: '900'
    }


});
