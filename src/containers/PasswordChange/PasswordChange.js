import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
    Platform, KeyboardAvoidingView, ScrollView,BackHandler
} from "react-native";
import styles from './styles'
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
class PasswordChange extends React.Component {
constructor(props) {
        super(props);
        this.state = {
            user_id: ''
        };
    };
    // componentWillMount(){
    //     BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
    // }
 render() {
        var padding
        Platform.select({
            ios: () => {
                padding = 'padding'
            },
            android: () => {
                padding = 0
            }
        })();
        return (
            <View style={styles.MainContainer}>
                <View style={{marginTop:'20%'}}>
                <View style={{ alignItems: 'center', }}>
                    <Text style={styles.text}>Password {"\n"} changed</Text>
                </View>
                <View style={styles.View2}>
                    <Image style={styles.Image1} 
                    source={require("../../components/icon/password.png")} />
                </View>
                <View style={{ alignItems: 'center',marginTop:'5%' }}>
                    <Text style={{ fontSize: RFValue(20), color: '#391EBB' }}>proceed to account creation</Text>
                </View>
                <TouchableOpacity onPress={() => Actions.Home()} style={{marginTop:'15%'}} >
                    <View style={styles.View3}>
                        <Text style={styles.text1}>CONTINUE</Text>
                    </View>
                </TouchableOpacity>
                </View>
            </View>
        );
    }
}


export default PasswordChange;
