import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#fff",
    },
    text: {
        color: '#391EBB',
        fontSize: RFValue(30),
        fontWeight: 'bold'
    },
    View2: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '7%'
    },
    Image1: {
        height: hp('35%'),
        width: wp('62%')
    },
    View3: {
        height: 50,
        backgroundColor: '#391EBB',
        justifyContent: 'center',
        alignItems: 'center',
        width: width - 80,
        alignSelf: 'center',
        borderRadius: 30,
    },
    text1: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: RFValue(15),
    }



});
