import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    View1: {
        backgroundColor: '#fff',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        marginTop: 20,
        height: height,
        flex: 1
    },
    View2: {
        flexDirection: 'row',
        paddingHorizontal: 20,
        marginTop: 20
    },
    View3: {
        marginLeft: 10,
        justifyContent: 'center'
    },
    text: {
        fontSize: RFValue(18),
        fontStyle: 'normal',
        color: '#505561'
    },
    Notifi_View:{
        justifyContent:'center',
        alignItems:'center',
        alignSelf:"center",
        marginTop:100,
        backgroundColor:'#fff'
    }


});
