import React from "react";
import { View, Text, StatusBar, Image, Dimensions, TextInput, ImageBackground, TouchableOpacity, Platform } from "react-native";
import { Actions } from 'react-native-router-flux';
import styles from './styles'
import { Card } from 'native-base';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
// import Dialog, { DialogContent } from 'react-native-popup-dialog';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
class ResetPassword extends React.Component {
 constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            Email: '',
            dialogclose: false,
        };
    };
    
    dialogopen() {
        this.setState({ dialogclose: true })
    }
    agreeBtn() {
        this.setState({ dialogclose: false })
        Actions.Login()
    }
    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: '#fff'
            }}>
                <View>
                    <AppHeader
                        icon={'arrow-back'}
                        backgo={require("../../components/Images/left.png")}
                        headerTitle={'Reset Password'}
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <View>
                    <Card style={{
                        backgroundColor: '#f2f2f2',
                        width: wp('85%'),
                        height: hp('7%'),
                        alignSelf: 'center',
                        flexDirection: 'row',
                        borderRadius: 5,
                        marginTop: 15,
                        paddingHorizontal: 10,
                        shadowOpacity: 1.5,
                        shadowColor: '#f2f2f2'
                    }}>
                        <View style={{ alignSelf: 'center', width: wp('82%'), }}>
                            <TextInput style={styles.textinput1}
                                underlineColorAndroid="transparent"
                                placeholder="Email"
                                placeholderTextColor="#999999"
                                autoCapitalize="none"
                                value={this.state.email}
                                onChangeText={(text) => this.setState({ email: text })} />
                        </View>
                    </Card>
                </View>
                <View style={{ flex: 1, marginTop: 10, width: width / 2 + 120, alignSelf: 'center' }}>
                    <Text style={{ textAlign: 'center', color: '#545454', }}>Input the email used to create your account. We will send you a link to reset your password </Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => this.dialogopen()}
                        style={{
                            backgroundColor: '#5fcb68', height: 50, width: '80%',
                            alignItems: 'center', justifyContent: 'center', borderRadius: 25, marginBottom: 30
                        }}>
                        <Text style={{ color: '#fff', fontWeight: 'bold' }}>Reset Password</Text>
                    </TouchableOpacity>
                </View>
                {/* <Dialog
                    visible={this.state.dialogclose}
                    onTouchOutside={() => {
                        this.setState({ dialogclose: true });
                    }}
                >
                    <DialogContent style={{
                        borderRadius: Platform.OS === 'android' ? null : null,
                        width: width/2+60,
                        height: 150,
                        paddingTop: 10

                    }}>
                         <View>
                            <View style={{ marginTop: 5, alignSelf: 'center' }}>
                                <Text style={{ color: '#5fcb68', fontWeight: 'bold', fontSize: 16 }}>Success!</Text>
                            </View>
                            <View style={{ marginTop: 10,width:width/2 + 30,alignSelf:'center'}}>
                                <Text style={{ textAlign: 'center',color:'#545454',fontSize:RF(2.2),}}>We have sent a reset password link to your email.</Text>
                            </View>
                            <View style={{ height: 1, width: width - 30, backgroundColor: 'grey', alignSelf: 'center',marginTop:20 }}></View>
                            <View style={{ marginTop: 10 }}>
                                <TouchableOpacity
                                   onPress={() => this.agreeBtn()}
                                >
                                    <View style={{alignSelf:'center'}}>
                                        <Text style={{fontSize: 15,color:'#5fcb68', fontWeight: 'bold'}}>OK</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                           
                           
                        </View>
                    </DialogContent>
                </Dialog> */}
            </View>
        );
    }
}
export default ResetPassword;
