import { StyleSheet, Dimensions,Platform } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export const COLOR = {
    DARK: "#040207",
    PANTOME: '#ff6f61',
    LIGHT: "#ffffff",
    BLACK: "#000",
    GRAY: "#9A9A9A",
    LIGHT_GRAY: "#ffffff",
    DANGER: "#FF5370",
    RED: "#800000",
    WHITE: "#FFF",
    CYAN: "#09818F",
    LIGHT_ORANGE: "#ff944d"
};
var padding
var paddingvet
    Platform.select({
      ios: () => {
        padding = 15
        paddingvet = null
      },
      android: () => {
        padding = 0
        paddingvet = 15
      }
    })();

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',

    },

    // main: {
    //     justifyContent: 'center',
    //     alignItems: 'center',

    // },

    text: {
        color: '#fff',
        fontSize: 11
    },

    view1: {
        marginTop: 5,
        marginLeft: 20,
        marginRight: 20
    },
    main: {
        justifyContent:'space-between',
         marginTop: 20,
         width: width / 1.3,
         backgroundColor: '#fff',
         shadowOpacity: 0.25,
         shadowRadius: 3.84,
         shadowOffset: { width: 0, height: 2, },
         borderRadius: 3,
         flexDirection: 'row',
         alignItems:'center',
         padding:padding,
         paddingHorizontal:paddingvet,
         alignSelf:'center'
     },

     image: {
        height: 15,
        width: 20,
        tintColor: '#fff',

        // marginLeft:15

    },
    textinput: {
        width: (width / 1.3 - 80 ),
        justifyContent: 'center',
        fontSize: 16,
        color: '#fff',
         textAlign: 'center',

    },

 



});   