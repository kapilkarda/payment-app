import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    SearchView: {
        flexDirection: 'row',
        borderColor: '#000',
        borderBottomWidth: 2,
        borderBottomColor: '#3F20DD80',
        height: 40,
        alignItems: 'center',
    },
    SearchImg: {
        height: 15,
        width: 15,
        marginLeft: 5,
        tintColor: '#3F20DD'
    },
    MainView: {
        backgroundColor: '#fff',
        marginTop: 25,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        height: width + 80,
    },
    QRView: {
        marginTop: '10%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    ShareView: {
        borderColor: '#391EBB',
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 30
    },
    ShareButton: {
        flexDirection: 'row',
        width: width / 2 - 20,
        height: 40,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    Footermain: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 120
    },
    FooterView: {
        width: '100%',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        backgroundColor: "#F3F3F3",

        position: 'absolute',
        bottom: 0
    },
    Footer: {
        backgroundColor: "#F3F3F3",
        borderTopWidth: 0,
        height: width / 5,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15
    },
    FooterTab:{
        backgroundColor: "#fff", 
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15
    },
    Home_img:{
        width: 45, height: 44, marginTop: 5
    }

});
