import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    Profile_View: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        marginTop: 10,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        height: hp('22%')
    },
    View_profile: {
        height: 30,
        width: 30,
        borderRadius: 30 / 2,
        borderWidth: 2,
        borderColor: '#fff',
        flex: 1,
        position: 'absolute',
        bottom: 8,
        backgroundColor: '#fff',
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        right: 6
    },
    Main_View: {
        backgroundColor: '#fff', marginTop: 10, borderTopLeftRadius: 15, borderTopRightRadius: 15, height: hp('65%')
    },
    Edit_text: {
        color: '#000', fontSize: RFValue(15), fontWeight: 'bold'
    },
    Name_View: {
        flexDirection: 'row', borderColor: 'grey', height: 50, borderWidth: 0.7,
        marginTop: 5, alignItems: 'center', paddingHorizontal: 10, borderRadius: 5
    },
    gradient:{
        backgroundColor: '#391EBB', height: 50, width: width - 40, alignSelf: 'center', marginTop: 20,
                                    justifyContent: 'center', alignItems: 'center', paddingHorizontal: 20, borderRadius: 30
    },
    name_text:{
        color: '#000', fontSize: RFValue(14) 
    }

  



});
