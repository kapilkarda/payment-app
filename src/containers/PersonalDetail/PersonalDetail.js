import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, FlatList, ScrollView, ImageBackground, ToastAndroid,
    Alert, BackHandler
} from "react-native";
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from '../../components/Loader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles'
class PersonalDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            filePath: {},
            email: '',
            name: '',
            mobile_no: '',
            imageurl: '',
            image: '',
            isLoading: false,
            images: false,
            Authoriz:''
        };

    };

    async componentWillMount() {
        const user_id = await AsyncStorage.getItem('user_id')
        const Authoriz = await AsyncStorage.getItem('Acess_token')
        console.log(user_id, "user_id")
        this.setState({
            user_id: user_id,
            Authoriz:Authoriz
        })
        this.getprofile(Authoriz)
        // BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
    }

    handlePressLogin = () => {
        this.Update()
    }

    //--------------- ---------Image_Picker-------------------//
    chooseFile = () => {
        var options = {
            title: 'Select Image',
            customButtons: [
                { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                let source = response;
                this.uploadImage(response.uri)
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    filePath: source,
                });
            }
        });
    };

    //--------------- ---------validation-------------------//
    Update() {
        if (this.state.name == "" || this.state.name == null || this.state.name == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter name'); },
                android: () => { ToastAndroid.show('Please enter name', ToastAndroid.SHORT); }
            })();
            return false;
        }
        const regp = /^[0]?[789]\d{9}$/;
        if (regp.test(this.state.mobile_no) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter mobile no'); },
                android: () => { ToastAndroid.show('Please enter mobile no', ToastAndroid.SHORT); }
            })();
            return false;
        }

        var text = this.state.email;
        let email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (email.test(text) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Email is Not Correct'); },
                android: () => { ToastAndroid.show('Email is Not Correct', ToastAndroid.SHORT); }
            })();
            return false;
        }
        this.PersonalDetail()
    }

    //--------------- ---------Update_Profile-------------------//

    PersonalDetail = () => {
        // if (this.state.images === true) {
        //     var img = this.state.imageurl.split('image/')
        // } else {
        //     var img = this.state.imageurl.split('images/')
        // }
        var img = this.state.imageurl.split('images/')
        this.setState({ isLoading: true })
        fetch('http://jokingfriend.com/Dero/index.php/Api/update_profile', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authoriz':this.state.Authoriz
            },
            body: JSON.stringify({
                "user_id": this.state.user_id,
                "image": img[1],
                "phone_number": '',
                "email_id": this.state.email,
                "name": this.state.name
            })
        })
            .then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === true || res.result === "true") {
                    this.setState({
                        isLoading: false
                    })
                    Actions.UserPro()
                }
            })
            .catch((e) => {
                console.log('Error BeauticianListAPI')
                console.warn(e);
            });
    };

    //--------------- ---------Image_Upload-------------------//
    uploadImage = async (image) => {
        this.setState({ isLoading: true })
        console.log('imagef', image)
        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
        };
        var data = new FormData();
        data.append('file', file);
        const serviceType = 'user'
        fetch(`http://jokingfriend.com/Dero/index.php/Api/image_upload`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.data) {
                    this.setState({
                        imageurl: res.full_data,
                        images: true,
                        isLoading: false
                    })
                }
            })
            .catch((e) => {
                console.log(e)
                Alert('Upload Failed')
            });
    };

    //--------------- ---------get_Profile-------------------//
    getprofile = (Authoriz) => {
        this.setState({ isLoading: true })
        fetch('http://jokingfriend.com/Dero/index.php/Api/get_profile', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authoriz':Authoriz
            },
            body: JSON.stringify({
                "user_id": this.state.user_id
            })
        })
            .then((res) => res.json())
            .then(res => {
                console.log(res)
                this.setState({
                    name: res.data.name,
                    email: res.data.email,
                    mobile_no: res.data.phone_number,
                    imageurl: res.data.image,
                    isLoading: false
                })
            })
            .catch((e) => {
                console.log('Error BeauticianListAPI')
                console.warn(e);
            });
    };

    render() {
        return (
            <View style={styles.MainContainer}>
                <AppHeader
                    headerTitle={'Personal Details'}
                    color={"#000"}
                    tintColor={'#391EBB'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <ScrollView>
                    <View style={{ backgroundColor: '#F3F3F3', marginTop: 10, }}>
                        <View style={styles.Profile_View}>
                            <TouchableOpacity onPress={this.chooseFile.bind(this)} >
                                <ImageBackground style={{ height: 100, width: 100, }}
                                    imageStyle={{ borderRadius: 100 / 2 }}
                                    source={this.state.filePath.data !== undefined ? {
                                        uri: 'data:image/jpeg;base64,' + this.state.filePath.data,
                                    } :
                                        { uri: this.state.imageurl }} >
                                    <View style={styles.View_profile}>
                                        <Image style={{ height: 22, width: 22, }}
                                            source={require("../../components/icon/photo-camera.png")} />
                                    </View>
                                </ImageBackground>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.Main_View}>
                            <View style={{ marginTop: 20, marginLeft: 20 }}>
                                <Text style={styles.Edit_text}>Edit Details</Text>
                            </View>
                            <View style={{ paddingHorizontal: 20, marginTop: 20 }}>
                                <Text style={styles.name_text}>Name</Text>
                                <View style={styles.Name_View}>
                                    <Image style={{ height: 18, width: 18 }}
                                        source={require("../../components/icon/editp.png")} />
                                    <View style={{ width: width - 80 }}>
                                        <TextInput style={{ marginLeft: 10 }}
                                            placeholder='Rustin David'
                                            placeholderTextColor='grey'
                                            returnKeyType="next"
                                            onChangeText={(text) => this.setState({ name: text })}
                                            value={this.state.name} />
                                    </View>
                                </View>
                            </View>
                            <View style={{ paddingHorizontal: 20, marginTop: 20 }}>
                                <Text style={styles.name_text}>Mobile Number</Text>
                                <View style={styles.Name_View}>
                                    <Image style={{ height: 20, width: 21 }}
                                        source={require("../../components/icon/reg7.png")} />
                                    <View style={{ width: width - 80 }}>
                                        <TextInput style={{ marginLeft: 10 }}
                                            placeholder='+01 2826XXX'
                                            placeholderTextColor='grey'
                                            maxLength={10}
                                            value={this.state.mobile_no} />
                                    </View>
                                </View>
                            </View>
                            <View style={{ paddingHorizontal: 20, marginTop: 20 }}>
                                <Text style={styles.name_text}>Email Address</Text>

                                <View style={styles.Name_View}>
                                    <Image style={{ height: 18, width: 19 }}
                                        source={require("../../components/icon/mail.png")} />
                                    <View style={{ width: width - 80 }}>
                                        <TextInput style={{ marginLeft: 10 }}
                                            placeholder='xyz123@gmail.com'
                                            placeholderTextColor='grey'
                                            returnKeyType='done'
                                            onChangeText={(text) => this.setState({ email: text })}
                                            value={this.state.email}
                                            onSubmitEditing={this.handlePressLogin} />
                                    </View>
                                </View>
                            </View>
                            <TouchableOpacity onPress={() => this.Update()}>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    colors={['#3E1EDD', '#5C48C5']}
                                    style={styles.gradient}>
                                    <Text style={{ color: '#fff', fontSize: RFValue(20) }}>UPDATE</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ height: 80, backgroundColor: '#F3f3f3' }}></View>
                </ScrollView>
                {
                    this.state.isLoading &&
                    <Spinner />
                }
            </View>
        );
    }
}


export default PersonalDetail;
