import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, FlatList, ScrollView
} from "react-native";
import { Footer, FooterTab, Button } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import OtpInputs from 'react-native-otp-inputs'
import Dialog, { DialogContent } from 'react-native-popup-dialog';
class TearmsService extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: '', dialogclose: false,

        };
        otpRef = React.createRef()

        clearOTP = () => {
            otpRef.current.clear()
        }
    };
    handlePressLogin = () => {
        this.OtpConfirm()
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#F3F3F3", }}>
                <AppHeader
                    headerTitle={'Terms & Services'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                    color={"#000"}
                />
                <View style={{ flex: 1, backgroundColor: '#fff', borderTopLeftRadius: 15, borderTopRightRadius: 15, marginTop: 15 }}>
                    <View style={{paddingHorizontal:15}} >
                        <View>
                            <Text style={{ color: '#000', fontSize: RFValue(18), marginTop: 15 }}>
                                Tearms and Services
        </Text>
                            <Text style={{ color: 'grey', fontSize: RFValue(10) }}> Last Updated 04 September,2019 </Text>
                        </View>
                        <View style={{marginTop:15,width:width-40}}>
                            <Text style={{ color: 'grey', fontSize: RFValue(13),textAlign:'justify' }}>
                                Lorem Ipsum is simply dummy text of the printing and
                                 typesetting industry. Lorem Ipsum has been the industry's
                                  standard dummy text ever since the 1500s,
                                  when an unknown printer took a galley of type
                                   and scrambled it to make a type specimen book.
        </Text>
                            <Text style={{ color: 'grey', fontSize: RFValue(13),marginTop:5,textAlign:'justify' }}>
                                Lorem Ipsum is simply dummy text of the printing and
                                 typesetting industry. Lorem Ipsum has been the industry's
                                  standard dummy text ever since the 1500s,
                                  when an unknown printer took a galley of type
                                   and scrambled it to make a type specimen book.
        </Text>
                            <Text style={{ color: 'grey', fontSize: RFValue(13),marginTop:5,textAlign:'justify' }}>
                                Lorem Ipsum is simply dummy text of the printing and
                                 typesetting industry. Lorem Ipsum has been the industry's
                                  standard dummy text ever since the 1500s,
                                  when an unknown printer took a galley of type
                                   and scrambled it to make a type specimen book.
        </Text>
                            <Text style={{ color: 'grey', fontSize: RFValue(13),marginTop:5,textAlign:'justify' }}>
                                Lorem Ipsum is simply dummy text of the printing and
                                 typesetting industry. Lorem Ipsum has been the industry's
                                  standard dummy text ever since the 1500s,
                                  when an unknown printer took a galley of type
                                   and scrambled it to make a type specimen book.
        </Text>
                            <Text style={{ color: 'grey', fontSize: RFValue(13),marginTop:5,textAlign:'justify' }}>
                                a. Lorem Ipsum is simply dummy text of the printing{"\n"}
                                b. Lorem Ipsum is simply dummy text of the printing
        </Text>
                        </View>

                        <View>
                            <Text style={{ color: '#000', fontSize: RFValue(18), marginTop: 15 }}>
                                Privacy pilicy
        </Text>
                            <Text style={{ color: '#61A1EE', fontSize: RFValue(15), marginTop: 5 }}>
                               http://dero.com/mypolicy/5627
        </Text>

                        </View>

                    </View>
                </View>


            </View>


        );
    }
}


export default TearmsService;
