import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, FlatList, ScrollView
} from "react-native";
import { Footer, FooterTab, Button } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import OtpInputs from 'react-native-otp-inputs'
import Dialog, { DialogContent } from 'react-native-popup-dialog';
class VerifyNo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: '', dialogclose: false,

        };
        otpRef = React.createRef()

        clearOTP = () => {
            otpRef.current.clear()
        }
    };
    handlePressLogin = () => {
        this.OtpConfirm()
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#F3F3F3", }}>
                <AppHeader
                    // headerTitle={'Help And Support'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <ScrollView>
                    <View style={{ flex: 1, marginTop: "40%" }} >
                        <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                            <View>
                                <Text style={{ color: '#391EBB', fontSize: RFValue(30) }}>Verify your mobile</Text>
                            </View>
                            <View style={{ alignItems: 'center', width: width / 2 + 100, marginTop: 10 }}>
                                <Text style={{ color: '#391EBB', fontSize: RFValue(14) }}>It is important to verify the mobile number that is registred against your account</Text>
                            </View>
                            <View style={{flex:1,marginTop:20}} >
                                <OtpInputs
                                    inputContainerStyles={{
                                        backgroundColor: "#391EBB",
                                        borderRadius: 10
                                    }}
                                    inputStyles={{ color: '#fff' }}
                                    ref={this.otpRef}
                                    handleChange={code => console.log(code)}
                                    numberOfInputs={4}
                                    onSubmitEditing={this.handlePressLogin}
                                    
                                />
                            </View>

                            <View style={{ marginTop: 40 }}>
                                <Text style={{ color: '#391EBB', fontSize: RFValue(15) }}>
                                    I have't received a code
                            </Text>
                            </View>

                        </View>
                    </View>
                    <View style={{ height: 50 }}></View>
                </ScrollView>


            </View>


        );
    }
}


export default VerifyNo;
