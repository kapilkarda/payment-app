import { StyleSheet } from 'react-native';
import { Colors } from '../../Theme';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export const COLOR = {
DARK: "#040207",
PANTOME: '#ff6f61',
LIGHT: "#ffffff",
BLACK: "#000",
GRAY: "#9A9A9A",
LIGHT_GRAY: "#ffffff",
DANGER: "#FF5370",
RED: "#800000",
WHITE:"#FFF",
CYAN: "#09818F",
LIGHT_ORANGE:"#ff944d"
};

export default StyleSheet.create({

});