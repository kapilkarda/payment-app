import { StyleSheet } from 'react-native';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export const COLOR = {
  DARK: "#040207",
  PANTOME: '#ff6f61',
  LIGHT: "#ffffff",
  BLACK: "#000",
  GRAY: "#9A9A9A",
  LIGHT_GRAY: "#ffffff",
  DANGER: "#FF5370",
  RED: "#800000",
  WHITE: "#FFF",
  CYAN: "#09818F",
  YELLOW: "#cccc00"
};

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ffffff'
  },
  Seach_View: {
    flexDirection: 'row',
    borderColor: '#000',
    borderBottomWidth: 2,
    borderBottomColor: '#3F20DD80',
    height: 40,
    alignItems: 'center',
  },
  Search_img: {
    height: 15,
    width: 15,
    marginLeft: 5,
    tintColor: '#3F20DD'
  },
  local_img: {
    height: hp('10%'),
    width: wp('16%'),
    resizeMode: 'contain',
  },
  local_text: {
    fontSize: RFValue(15),
    color: '#3F20DD',
    textAlign: 'center',
    marginTop: 5
  },
  QR_img: {
    height: hp('10%'),
    width: wp('16%'),
    resizeMode: 'contain'
  },
  Mainlist_View: {
    marginTop: 40,
    backgroundColor: '#fff',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  My_Contact:{
    fontSize: RFValue(15), color: '#3F20DD', fontWeight: 'bold'
  }
});
