import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity,
    FlatList, TextInput, ScrollView, PermissionsAndroid, BackHandler
} from "react-native";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import Contacts from 'react-native-contacts';
import Spinner from '../../components/Loader';
import LinearGradient from "react-native-linear-gradient";
import AsyncStorage from '@react-native-community/async-storage';
import styles from "./styles";

class SendMoney extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            contacts: null,
            searching: '',
            email: '',
            returnAllContacts: [],
            isLoading: false,
            localContact: true,
            globalContact: false,
            globleList:[]
        };
        this.searchContact = this.searchContact.bind(this);
    };
    //--------------- ---------get ALl Contact-------------------//
    async componentDidMount() {
        const user_id = await AsyncStorage.getItem('user_id')
        const Authoriz = await AsyncStorage.getItem('Acess_token')
        this.setState({
            user_id: user_id
        })
        isLoading = true
        let contactList = []
        if (Platform.OS === 'ios') {
            Contacts.getAll((err, contacts) => {
                if (err) {
                    throw err;
                }
                // contacts returned
                this.setState({ contacts })
            })
        } else if (Platform.OS === 'android') {
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                    title: 'Contacts',
                    message: 'This app would like to view your contacts.'
                }
            ).then(() => {
                Contacts.getAll((err, contacts) => {
                    if (err === 'denied') {
                        // error
                    } else {
                        for (let item of contacts) {
                            if (item.phoneNumbers.length != 0) {
                                contactList.push({ "user_name": item.displayName, "mobile_number": item.phoneNumbers[0].number, "email": "a@gmail.com" })
                            }
                        }
                        // contacts returned in Array
                        this.setState({ contacts })
                    }
                })
            })
        }
        setTimeout(() => {
            this.inviteButton(contactList,Authoriz)
        }, 3000);
    }
    //--------------- ---------Api invite button-------------------//
    inviteButton = (contactList,Authoriz) => {
        this.setState({ isLoading: true })
        const phoneContacts = {
            "user_id": this.state.user_id,
            "contact": contactList,
        }
        fetch('http://jokingfriend.com/Dero/index.php/Api/phoneContacts', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authoriz':Authoriz
            },
            body: JSON.stringify(phoneContacts)
        })
            .then((res) => res.json())
            .then(res => {

               
                console.log(res)

                if (res.result === true || res.result === "true") {
                    var globle = []
                    for (let item of res.data) {
                        if(item.status == 1){
                            globle.push({
                                contact: item.contact,
                                email:item.email,
                                name: item.name,
                                status: item.status,
                            })
                        }
                        
                    }
                    this.setState({
                        isLoading: false,
                        returnAllContacts: res.data,
                        globleList:globle
                    }, () => {
                        console.log(this.state.returnAllContacts)
                    })
                }
                else {
                    alert(res.msg)
                    this.setState({
                        isLoading: false
                    })
                }
            })
            .catch((e) => {
                console.log('Oops')
                console.warn(e);
            });
    };
    //------------------------Local Button-------------------//

    LocalContactList = () => {
        this.setState({
            globalContact: false,
            localContact: true,
        })
    }

    globalContactLsit = () => {
        for (let connectContact of this.state.returnAllContacts) {
            if (connectContact.status === 1) {
                console.log(connectContact.status)
                this.setState({
                    globalContact: true,
                    localContact: false
                })
            }
        }
    }

    //--------------- ---------Local Search-------------------//
    searchContact(contacts) {
        console.log(contacts)
        this.setState({
            searching: contacts
        })
    }

    render() {
        let filterContactData = [];
        let GlobalFilter = [];
        if (filterContactData) {
            filterContactData = this.state.searching ? this.state.returnAllContacts.filter(row => row.name.toLowerCase().indexOf(this.state.searching.toLowerCase()) > -1) : this.state.returnAllContacts;
        }
        if (GlobalFilter) {
            GlobalFilter = this.state.searching ? this.state.globleList.filter(row => row.name.toLowerCase().indexOf(this.state.searching.toLowerCase()) > -1) : this.state.globleList;
        }
        console.log(filterContactData, "filterContactData")
        return (
            <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
                <AppHeader
                    headerTitle={'Send Money'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                    color={"#636363"}
                />
                <ScrollView>
                    <View style={{ backgroundColor: '#f3f3f3', marginTop: 10 }}>
                        {
                            (this.state.contacts !== 0) &&
                            <View style={{ marginTop: 10, paddingHorizontal: 20 }}>
                                <View style={styles.Seach_View}>
                                    <View>
                                        <Image resizeMode="contain"
                                            style={styles.Search_img}
                                            source={require("../../components/icon/search1.png")} />
                                    </View>
                                    <View style={{ width: width - 70 }}>
                                        <TextInput style={{ marginLeft: 10 }}
                                            placeholder='Search name or number'
                                            placeholderTextColor='grey'
                                            onChangeText={(text) => this.searchContact(text)}
                                        />
                                    </View>
                                </View>
                            </View>
                        }
                        <View style={{ marginTop: 20 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                                <TouchableOpacity onPress={() => this.LocalContactList()}>
                                    <View>
                                        <Image style={styles.local_img}
                                            source={require("../../components/icon/send2.png")} />
                                        <Text style={styles.local_text}>Local</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.globalContactLsit()} >
                                    <View>
                                        <Image style={styles.local_img}
                                            source={require("../../components/icon/home7.png")} />
                                        <Text style={styles.local_text}>Global</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Actions.QrScan()}>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image style={styles.QR_img}
                                            source={require("../../components/icon/send3.png")} />
                                        <Text style={styles.local_text}>QR Code</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View style={styles.Mainlist_View}>
                        <View style={{ justifyContent: 'center', }}>
                            <View style={{ marginTop: 20, marginLeft: 30, }}>
                                <Text style={styles.My_Contact}>My Contacts</Text>
                                <View style={{ height: 2, width: '24.5%', backgroundColor: '#3F20DD', marginTop: 3 }}></View>
                            </View>
                            {
                                this.state.localContact &&
                                <View>
                                    {
                                        filterContactData && filterContactData.map((item, i) => (
                                            <View key={i} style={{ marginTop: 10, }}>
                                                <View>
                                                    <View style={{ paddingHorizontal: 20 }}>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                            <TouchableOpacity onPress={() => Actions.PaymentSend({ Contact: item })}>
                                                                <View style={{ flexDirection: 'row', width: width / 2 + 70, }}>
                                                                    <View>
                                                                        <Image style={{ height: 50, width: 50, resizeMode: 'contain' }}
                                                                            source={require("../../components/icon/req5.png")}/>
                                                                    </View>

                                                                    <View style={{ justifyContent: 'center', marginLeft: 10 }}>
                                                                        <Text style={{ fontSize: RFValue(16) }}>{item.name}</Text>

                                                                        <Text style={{ fontSize: RFValue(12), color: '#000' }}>{item.contact}</Text>

                                                                    </View>
                                                                </View>
                                                            </TouchableOpacity>
                                                            {
                                                                item.status == 0 &&
                                                                <TouchableOpacity style={{ justifyContent: 'center', }} >
                                                                    <LinearGradient
                                                                        start={{ x: 0, y: 0 }}
                                                                        end={{ x: 1, y: 0 }}
                                                                        colors={['#3E1EDD', '#5C48C5']}
                                                                        style={{
                                                                            justifyContent: 'center',
                                                                            width: 60, height: 25,
                                                                            alignItems: 'center',
                                                                            backgroundColor: "#391EBB", borderRadius: 15
                                                                        }}>
                                                                        <Text style={{ fontSize: RFValue(12), color: '#fff', fontWeight: '900' }}>Invite</Text>
                                                                    </LinearGradient>
                                                                </TouchableOpacity>
                                                            }
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        ))
                                    }
                                </View>
                            }

                            {
                                this.state.globalContact &&
                                <View>
                                    {
                                        GlobalFilter && GlobalFilter.map((item, i) => (
                                            <View style={{ marginTop: 10 }} key={i}>
                                                <View style={{ paddingHorizontal: 20 }}>
                                                   
                                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                        <TouchableOpacity onPress={() => Actions.PaymentSend({ Contact: item })}>
                                                            <View style={{ flexDirection: 'row', width: width / 2 + 70, }}>
                                                                <View>
                                                                    <Image style={{ height: 50, width: 50, resizeMode: 'contain' }}
                                                                        source={require("../../components/icon/home13.png")}></Image>
                                                                </View>

                                                                <View style={{ justifyContent: 'center', marginLeft: 10 }}>
                                                                    <Text style={{ fontSize: RFValue(16) }}>{item.name}</Text>
                                                                    <Text style={{ fontSize: RFValue(12), color: '#000' }}>{item.contact}</Text>
                                                                </View>
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>

                                            </View>
                                        ))
                                    }
                                </View>
                            }
                        </View>
                    </View>
                </ScrollView>
                {
                    this.state.isLoading &&
                    <Spinner />
                }
            </View>
        );
    }
}

export default SendMoney;
