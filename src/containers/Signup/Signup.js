import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
    ScrollView, ListView, FlatList,
} from "react-native";
import styles from './styles';
import AppHeader from '../../components/AppHeader';
import { Actions } from "react-native-router-flux";
import RF from "react-native-responsive-fontsize"
import { Card } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

let data = [{
    value: 'Personal User',
}, {
    value: 'Business User',
},
];

class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            email: '',
            password: '',
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <AppHeader
                    headerTitle={'Sign Up'}
                    backgo={require("../../components/Images/leftarrow.png")}
                    onPress={() => this.props.navigation.goBack()}
               
                />
                <ScrollView>
                    <View>
                        <View style={styles.parentViewStyle}>
                            <Card style={styles.dropDownCard}>
                                <View style={styles.dropDownViewStyle}>
                                    <Dropdown
                                        containerStyle={{ justifyContent: 'center' }}
                                        dropdownPosition={-3}
                                        inputContainerStyle={{ borderBottomColor: 'transparent', marginBottom: 5 }}
                                        label='Select User Type'
                                        data={data}
                                    />
                                </View>
                            </Card>
                        </View>
                        <View>
                            <Card style={{
                                backgroundColor: '#fff',
                                width: wp('85%'),
                                height: hp('7%'),
                                alignSelf: 'center',
                                flexDirection: 'row',
                                borderRadius: 5,
                                marginTop: 5,
                                paddingHorizontal: 10,
                                shadowOpacity: 1.5,
                                shadowColor: '#545454'
                            }}>
                                <View style={{ alignSelf: 'center', width: wp('82%'), }}>
                                    <TextInput style={styles.textinput1}
                                        underlineColorAndroid="transparent"
                                        placeholder="Email"
                                        placeholderTextColor="#999999"
                                        autoCapitalize="none"
                                        value={this.state.email}
                                        onChangeText={(text) => this.setState({ email: text })} />
                                </View>
                            </Card>
                        </View>
                        <View style={styles.view4}>
                            <Image source={require("../../components/Images/tick.png")}
                                style={styles.image3}>
                            </Image>
                            <Text style={styles.text2}>Valid email.</Text>
                        </View>
                        <View style={styles.view6}>
                            <Image source={require("../../components/Images/tick.png")}
                                style={styles.image3}>
                            </Image>
                            <Text style={styles.text2}>Not in use.</Text>
                        </View>

                        <View>
                            <Card style={{
                                backgroundColor: '#fff',
                                width: wp('85%'),
                                height: hp('7%'),
                                alignSelf: 'center',
                                flexDirection: 'row',
                                borderRadius: 5,
                                marginTop: 10,
                                paddingHorizontal: 10,
                                shadowOpacity: 1.5,
                                shadowColor: '#545454'
                            }}>
                                <View style={{ alignSelf: 'center', width: wp('82%'), }}>
                                    <TextInput style={styles.textinput1}
                                        underlineColorAndroid="transparent"
                                        placeholder="Password"
                                        placeholderTextColor="#999999"
                                        autoCapitalize="none"
                                        secureTextEntry={true}
                                        value={this.state.password}
                                        onChangeText={(text) => this.setState({ password: text })} />
                                </View>
                            </Card>
                        </View>
                        <View style={styles.view4}>
                            <Image source={require("../../components/Images/tick.png")}
                                style={styles.image3}>
                            </Image>
                            <Text style={styles.text2}>At least 6 characters long.</Text>
                        </View>
                        <View style={styles.view6}>
                            <Image source={require("../../components/Images/tick.png")}
                                style={styles.image3}>
                            </Image>
                            <Text style={styles.text2}>Contains a letter.</Text>
                        </View>
                        <View style={styles.view6}>
                            <Image source={require("../../components/Images/tick.png")}
                                style={styles.image3}>
                            </Image>
                            <Text style={styles.text2}>Contains a number.</Text>
                        </View>
                        <View style={styles.view6}>
                            <Image source={require("../../components/Images/tick.png")}
                                style={styles.image3}>
                            </Image>
                            <Text style={styles.text2}>Contains a special character.</Text>
                        </View>
                    </View>
                    <View>
                        <View style={{ marginTop: 10, alignSelf: 'center' }}>
                            <TouchableOpacity
                                onPress={() => Actions.Login()}
                                style={styles.next}>
                                <Text style={{ color: '#fff', fontSize: 14, fontWeight: 'bold' }}>Sign Up</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 20 }}>
                            <Text style={{ color: '#000', fontWeight: 'bold',  }}>Already have an account?</Text>
                            <TouchableOpacity
                                onPress={() => Actions.Login()}
                            >
                                <Text style={{ color: '#5fcb68', fontWeight: 'bold', marginLeft: 5 }}>LOGIN HERE!</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>

        );
    }
}

export default Signup;