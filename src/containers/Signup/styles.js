import { StyleSheet, Dimensions,Platform } from 'react-native'; Dimensions
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import Dialog, { DialogContent } from 'react-native-popup-dialog';
// import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export const COLOR = {
    DARK: "#040207",
    PANTOME: '#ff6f61',
    LIGHT: "#ffffff",
    BLACK: "#000",
    GRAY: "#9A9A9A",
    LIGHT_GRAY: "#ffffff",
    DANGER: "#FF5370",
    RED: "#800000",
    WHITE: "#FFF",
    CYAN: "#09818F",
    LIGHT_ORANGE: "#ff944d"
};

var padding
var paddingvet
    Platform.select({
      ios: () => {
        padding = 15
        paddingvet = null
      },
      android: () => {
        padding = null
        paddingvet = 15
      }
    })();

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',

    },

    parentViewStyle: {
        marginVertical: 10,paddingHorizontal:10,
      },
      dropDownViewStyle: {
        bottom: 15,
        paddingHorizontal:10,
      },
    
      card: {
        marginLeft: 18,
        marginRight: 18,
        marginTop: 10,
        height: 50,
        padding:10,
        backgroundColor:'#fff',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        shadowOpacity: 1.5,
        shadowColor: '#545454'
      },
    
      dropDownCard: {
        marginLeft: 18,
        marginRight: 18,
        height: 50,
        justifyContent: 'flex-start',
        shadowOpacity: 1.5,
        shadowColor: '#545454'
      },
      
    view4: {
        flexDirection: 'row',
        marginLeft: '12%',
        marginTop: 10
    },
    view5: {
        backgroundColor: '#fff', 
        width: wp('70%'), 
        height: hp('6%'),
        alignSelf: 'center', 
        borderRadius: 3, 
        marginTop: 20,
        flexDirection:'row',
        paddingHorizontal:10
    },
    view6: {
        flexDirection: 'row',
        marginLeft: '12%',
        marginTop: 5
    },
    image3: {
        height: hp('2%'),
        width: wp('4%'),
        tintColor:'#000'
    },
    text2: {
        color: '#000',
        // fontSize: RF(2),
        marginLeft: 10
    },
    next:{
        width: wp('70%'), 
        height: hp('7%'), 
        marginTop: hp('20%'),
        backgroundColor: '#5fcb68', 
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25
    },

});