import { StyleSheet, Dimensions,Platform } from 'react-native'; 
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


export const COLOR = {
    BLACK: "#000",
    GRAY: "#9A9A9A",
    WHITE: "#FFF",
    LIGHT_ORANGE: "#E69151"
};
var padding
var paddingvet
    Platform.select({
      ios: () => {
        padding = 15
        paddingvet = null
      },
      android: () => {
        padding = 0
        paddingvet = 15
      }
    })();
export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
  

});