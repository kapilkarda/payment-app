import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
    ScrollView, ListView, FlatList,
} from "react-native";
import styles from './styles';
import AppHeader from '../../components/AppHeader';
import { Actions } from "react-native-router-flux";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <AppHeader
                    headerTitle={'Profile'}
                    image1={require("../../components/Images/homeleftheader.png")}
                    drawer={() => Actions.drawerOpen()}
                 // onPress={() => this.props.navigation.goBack()}
                // userprofile={require("../../components/Images/homerightheader.png")}
                // profile={() => Actions.Setlocation()}
                />
                <ScrollView>
                    <View>
                        <View style={{ alignSelf: 'center', marginTop: 20 }}>
                            <Image source={require("../../components/Images/Profile1.png")}
                                style={{ width: wp('35%'), height: hp("25%"), marginTop: 3 }}>
                            </Image>
                        </View>
                        <View style={{ alignSelf: 'center', marginTop: 20 }}>
                            <TouchableOpacity
                             onPress={() => Actions.Addcard()}
                            >
                                <Image source={require("../../components/Images/Profile2.png")}
                                    style={{ width:width, height: hp("5%"), marginTop: 3 }}>
                                </Image>
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View>
                                    <Image source={require("../../components/Images/visa.png")}
                                        style={{ width: wp('14%'), height: hp("4.5"), marginTop: 3 }}>
                                    </Image>
                                </View>
                                <View style={{ marginTop: 3, marginLeft: 8 }}>
                                    <Text style={{  fontWeight: 'bold', color: '#545454' }}>VISA</Text>
                                    <Text style={{  color: '#545454' }}>**** **** **** 1234</Text>
                                </View>
                            </View>
                            <View>
                                <TouchableOpacity>
                                    <Image source={require("../../components/Images/Cross.png")}
                                        style={{ width: wp('4%'), height: hp("3%"), marginTop: 3 }}>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{
                            height: 1,
                            width: width-30,
                            backgroundColor: '#eeeeee',
                            marginTop: 10,
                            alignSelf: 'center', marginLeft: 5
                        }}>
                        </View>
                        <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View>
                                    <Image source={require("../../components/Images/american.png")}
                                        style={{ width: wp('15%'), height: hp("5%"), marginTop: 3 }}>
                                    </Image>
                                </View>
                                <View style={{ marginTop: 3, marginLeft: 8 }}>
                                    <Text style={{  fontWeight: 'bold', color: '#545454' }}>AMERICAN EXPRESS</Text>
                                    <Text style={{ color: '#545454' }}>**** **** **** 1234</Text>
                                </View>
                            </View>
                            <View>
                                <TouchableOpacity>
                                    <Image source={require("../../components/Images/Cross.png")}
                                        style={{ width: wp('4%'), height: hp("3%"), marginTop: 3 }}>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{
                            height: 1,
                            width: width - 30,
                            backgroundColor: '#eeeeee',
                            marginTop: 10,
                            alignSelf: 'center', marginLeft: 5
                        }}>
                        </View>
                    </View>
                </ScrollView>
            </View>

        );
    }
}


export default Profile;