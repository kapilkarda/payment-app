import { StyleSheet, Dimensions } from 'react-native';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    ProfileView: {
        backgroundColor: '#fff',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        marginTop: 10,
        height: width / 2,
        marginTop: 20
    },
    detailView: {
        width: width,
        backgroundColor: '#fff',
        height: width / 2 - 80,
        marginTop: 10
    },
    text: {
        color: '#000', fontSize: RFValue(15)
    },
    notificationView: {
        width: width,
        backgroundColor: '#fff',
        height: width / 2 - 90,
        marginTop: 10
    },
    footerView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 120
    },
    footerView2: {
        width: '100%',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        backgroundColor: "#F3F3F3",
        //   justifyContent: 'center', 
        //   alignItems: 'center',
        position: 'absolute',
        bottom: 0
    },
    footer: {
        backgroundColor: "#F3F3F3",
        borderTopWidth: 0,
        height: width / 5,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15
    },
    footertab: {
        backgroundColor: "#fff",
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15
    },
    Dialog: {
        width: width - 80,
        height: width / 2 - 20,
        paddingTop: 10,
    },
    DialogText: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: RFValue(18)
    },
    DialogView: {
        alignSelf: 'flex-end',
        flexDirection: 'row',
        marginTop: 70,
        justifyContent: 'space-between',
        width: width / 3 + 20
    },
    helpimg: {
        height: 25,
        width: 25,
        marginLeft: 5
    },
    notiimg: { 
        height: 27, 
        width: 25, 
        marginLeft: 5 
    },
    arrowimg:{
        height: 20,
         width: 15,
          tintColor: '#000'
    },
    signoutView:{
        width: width,
         backgroundColor: '#fff',
          height: width / 2 - 130, 
          marginTop: 10
    }
});