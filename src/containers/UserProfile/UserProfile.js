import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, Platform, AlertIOS, alert, Alert,
    FlatList, ScrollView, BackHandler, AsyncStorage
} from "react-native";
import styles from './styles'
import { Footer, FooterTab, Button } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import Spinner from '../../components/Loader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            dialogclose: false, image: '',
            isLoading: false
        };
    };

    logout() {
        Alert.alert('Logout', 'Are you sure you want to logout ?', [
            { text: 'CANCEL', onPress: () => console.log('CANCEL Pressed'), style: 'cancel' },
            {
                text: 'LOGOUT', onPress: () => {
                    AsyncStorage.clear()
                    Actions.SignIn()
                }
            },
        ])
    }
    async componentDidMount() {
        // BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
        const user_id = await AsyncStorage.getItem('user_id')
        const Authoriz = await AsyncStorage.getItem('Acess_token')
       

        this.getprofile(user_id,Authoriz)
    }

    handleBackButtonClick = () => {
        this.setState({ dialogclose: false })
        this.setState({ dialogclose1: false })
        return true;
    }
    dialogopen() {
        this.setState({ dialogclose: true })
    }
    dialogclosebutton() {
        this.setState({ dialogclose: false })
        this.setState({ dialogclose1: false })
        // Actions.Login()
    }
    fun() {
        Actions.SignIn()
        this.setState({ dialogclose: false })
    }
    getprofile = (user_id,Authoriz) => {
        this.setState({ isLoading: true })
        fetch('http://jokingfriend.com/Dero/index.php/Api/get_profile', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authoriz':Authoriz
            },
            body: JSON.stringify({
                "user_id": user_id
            })
        })
            .then((res) => res.json())
            .then(res => {
                console.log(res)
                // const img = res.data.image.split('image/')

                this.setState({
                    name: res.data.name,
                    image: res.data.image,
                    isLoading: false
                })
                console.log(this.state.image, "image")
            })
            .catch((e) => {
                console.log('Oops')
                console.warn(e);
            });
    };


    render() {
        console.log(this.state.image, "image")
        return (
            <View style={styles.container}>
                <AppHeader
                    headerTitle={'Profile'} />
                <ScrollView>
                    <View style={styles.ProfileView}>
                        <View style={{ alignItems: 'center' }}>
                            <View style={{ alignItems: 'center', marginTop: 15, }}>
                                
                                <Image style={{ height: width / 3, width: width / 3, borderRadius: 60 }}

                                   
                                    source={{ uri: this.state.image }} />
                                <Text style={{ color: '#000', fontSize: RFValue(20) }}>{this.state.name}</Text>
                            </View>
                        </View>
                    </View>
                    <ScrollView>
                        <View style={styles.detailView}>
                            <View style={{ paddingHorizontal: 20, marginTop: 10, }}>
                                <TouchableOpacity onPress={() => Actions.PersonalDetail()}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View >
                                                <Image style={{ height: 35, width: 35, }}
                                                    source={require('../../components/icon/circle.png')} />

                                            </View>
                                            <View style={{ justifyContent: 'center', marginLeft: 15 }}>
                                                <Text style={styles.text}>
                                                Personal Details
                                </Text>
                                            </View>
                                        </View>
                                        <View style={{ justifyContent: 'center' }}>
                                            <Image style={styles.arrowimg}
                                                source={require('../../components/icon/rightarrow.png')} />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Actions.MyBank()}>
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        marginTop: 15
                                    }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{ marginLeft: 5 }}>
                                                <Image style={{ height: 25, width: 26, }}
                                                    source={require('../../components/icon/Icon.png')} />
                                            </View>
                                            <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                                                <Text style={styles.text}>
                                                    Add Bank Account
                                </Text>
                                            </View>
                                        </View>
                                        <View style={{ justifyContent: 'center' }}>
                                            <Image style={styles.arrowimg}
                                                 source={require('../../components/icon/rightarrow.png')}></Image>
                                        </View>

                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                    <View style={[styles.detailView, { height: width / 2 - 90, }]}>
                        <View style={{ paddingHorizontal: 20, marginTop: 10, }}>
                            <TouchableOpacity onPress={() => Actions.HelpSupport()}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View>
                                            <Image style={styles.helpimg}
                                                source={require('../../components/icon/user3.png')} />
                                        </View>
                                        <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                                            <Text style={styles.text}>
                                                Help & Support
                                </Text>
                                        </View>
                                    </View>
                                    <View style={{ justifyContent: 'center' }}>
                                        <Image style={styles.arrowimg}
                                            source={require('../../components/icon/rightarrow.png')} />
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Actions.TearmsService()}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                                <View style={{ flexDirection: 'row' }}>
                                   
                                        <View>
                                            <Image style={{ height: 26, width: 25, marginLeft: 5 }}
                                                source={require('../../components/icon/user4.png')} />
                                        </View>
                                  
                                    <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                                        <Text style={styles.text}>
                                            Terms & Services
                                </Text>
                                    </View>
                                </View>
                                <TouchableOpacity >
                                    <View style={{ justifyContent: 'center' }}>
                                        <Image style={styles.arrowimg}
                                            source={require('../../components/icon/rightarrow.png')} />
                                    </View>
                                </TouchableOpacity>

                            </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.notificationView}>
                        <View style={{ paddingHorizontal: 20, marginTop: 10, }}>
                            <TouchableOpacity onPress={() => Actions.Notification()}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View>
                                            <Image style={styles.notiimg}
                                                source={require('../../components/icon/profile4.png')} />
                                        </View>
                                        <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                                            <Text style={styles.text}>
                                                Notifications
                                </Text>
                                        </View>
                                    </View>
                                    <View style={{ justifyContent: 'center' }}>
                                        <Image style={styles.arrowimg}
                                             source={require('../../components/icon/rightarrow.png')} />
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Actions.ChangePassword()}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View>
                                            <Image style={{ height: 30, width: 25, marginLeft: 5 }}
                                                source={require('../../components/icon/lock.png')}/>
                                        </View>
                                        <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                                            <Text style={styles.text}>
                                                Change Password
                                </Text>
                                        </View>
                                    </View>
                                    <View style={{ justifyContent: 'center' }}>
                                        <Image style={styles.arrowimg}
                                            source={require('../../components/icon/rightarrow.png')}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.signoutView}>
                        <View style={{ paddingHorizontal: 20, marginTop: 10, }}>
                            <TouchableOpacity onPress={() => this.logout()}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View>
                                            <Image style={{ height: 30, width: 30, marginLeft: 5 }}
                                                source={require('../../components/icon/signout1.png')} />
                                        </View>
                                        <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                                            <Text style={styles.text}>
                                                Sign Out
                                </Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ height: 100, backgroundColor: 'F3F3F3' }} />
                </ScrollView>


                {/* <View style={styles.footerView}>
                    <View style={styles.footerView2}>
                        <Footer style={styles.footer} >
                            <FooterTab style={styles.footertab}>
                                <Button onPress={() => Actions.Home()} >
                                    <Image resizeMode={'contain'}
                                        style={{ width: 45, height: 44, marginTop: 5 }}
                                        source={require('../../components/icon/footer5.png')} />
                                    <Text style={{ fontSize: RFValue(15), color: '#000', bottom: 5 }}>Home</Text>
                                </Button>
                                <Button onPress={() => Actions.Transaction()} >
                                    <Image resizeMode={'contain'}
                                        style={{ width: 38, height: 35, marginTop: 5 }}
                                        source={require('../../components/icon/home_2.png')} />
                                    <Text style={styles.text}>
                                        Transaction
                                        </Text>
                                </Button>
                                <Button onPress={() => Actions.Request()}>
                                    <Image resizeMode={'contain'}
                                        style={{ width: 36, height: 35, marginTop: 5 }}
                                        source={require('../../components/icon/HOME-3.png')} />
                                    <Text style={styles.text}>
                                        Request
                                        </Text>
                                </Button>
                                <Button onPress={() => Actions.UserProfile()}>
                                    <Image resizeMode={'contain'}
                                        style={{ width: 32, height: 32, marginTop: 8 }}
                                        source={require('../../components/icon/a.png')} />
                                    <Text style={styles.text}>
                                        Account
                                        </Text>
                                </Button>
                            </FooterTab>
                        </Footer>
                    </View>
                </View> */}
                <Dialog
                    visible={this.state.dialogclose}
                    onTouchOutside={() => {
                        this.setState({ dialogclose: false });
                    }}>
                    <DialogContent style={[styles.Dialog, { borderRadius: Platform.OS === 'android' ? null : null, }]}>
                        <View>
                            <View>
                                <Text style={styles.DialogText}>Logout</Text>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text style={{ color: '#351EBB', fontSize: RFValue(18), }}>Are you sure Want to SignOut ?</Text>
                            </View>
                            <View style={styles.DialogView}>
                                <TouchableOpacity onPress={() => this.dialogclosebutton()}>
                                    <View>
                                        <Text style={{ color: '#000' }}>CANCEL</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.fun()}>
                                    <View>
                                        <Text style={{ color: '#000' }}>CONFIRM</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </DialogContent>
                </Dialog>
                {this.state.isLoading &&
                    <Spinner />
                }
            </View>
        );
    }
}


export default Home;
