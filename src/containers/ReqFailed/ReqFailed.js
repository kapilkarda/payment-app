import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, BackHandler
} from "react-native";

import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles'
class PaymentFailed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: ''
        };
    };
    render() {
        return (
            <View style={styles.Main_Container}>
                <AppHeader
                    headerTitle={'Request Money'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                    color={"#636363"}
                />

                <View style={styles.Main_view}>
                    <View style={styles.Logo_View}>
                        <Image style={styles.failed_img}
                            source={require('./../../components/icon/paymentf.png')} />
                    </View>
                    <View style={styles.View_text}>
                        <Text style={{
                            color: '#000',
                            fontSize: RFValue(20)
                        }}>
                            Request Failed
                                </Text>
                        <Text style={{
                            color: '#ABABAB',
                            fontSize: RFValue(14),
                            marginTop: 10
                        }}>
                            Your transaction was rejected by the
                        </Text>
                        <Text style={{
                            color: '#ABABAB',
                            fontSize: RFValue(14),
                        }}>
                            vendor. Please try again
                        </Text>
                    </View>
                    <TouchableOpacity style={styles.tochable}>
                        <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            colors={['#3E1EDD', '#5C48C5']}
                            style={styles.gradient}>
                            <Text style={styles.tochable_text}>
                                RETRY
                                    </Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>


        );
    }
}


export default PaymentFailed;
