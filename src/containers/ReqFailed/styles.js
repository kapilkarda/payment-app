import { StyleSheet, Dimensions } from 'react-native';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
    Main_Container: {
        flex: 1, backgroundColor: "#F3F3F3",
    },
    Main_view: {
        backgroundColor: '#fff',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        marginTop: 20,
        height: height,
        flex: 1,
    },
    Logo_View: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '20%'
    },
    failed_img: {
        height: width / 2 + 50,
        width: width / 2 + 80
    },
    View_text: {
        width: width / 2 + 40,
        alignSelf: 'center',
        alignItems: 'center',
    },
    tochable: {
        flex: 1,
        position: 'absolute',
        bottom: 60,
        alignSelf: 'center'
    },
    gradient: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        backgroundColor: '#391EBB',
        width: width - 50,
        borderRadius: 30
    },
    tochable_text: {
        color: '#fff',
        fontSize: RFValue(20),
        fontWeight: '600'
    }
});
