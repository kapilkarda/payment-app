import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, ScrollView, FlatList, PermissionsAndroid, BackHandler
} from "react-native";
import styles from './styles'
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { CameraKitCameraScreen, CAMERA } from 'react-native-camera-kit';
import LinearGradient from "react-native-linear-gradient";

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const data = [
    {
        imgage: require("./../../components/icon/qr1.png"),
        bankname: 'AWIns',
    },
    {
        imgage: require("./../../components/icon/qr1.png"),
        bankname: 'AWIns',

    },
    {
        imgage: require("./../../components/icon/qr1.png"),
        bankname: 'AWIns',

    },

]
class QrScan extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            QR_Code_Value: '',
            Start_Scanner: false,
        };
    };
    componentWillMount() {
        this.open_QR_Code_Scanner()

    }
    openLink_in_browser = () => {
        Linking.openURL(this.state.QR_Code_Value);
    }
    onQR_Code_Scan_Done = (QR_Code) => {

        this.setState({ QR_Code_Value: QR_Code });
        alert(QR_Code)
        Actions.PaymentSend({ QRvalue: this.state.QR_Code_Value })
        console.log(this.state.QR_Code_Value)
        this.setState({ Start_Scanner: false });
    }
    open_QR_Code_Scanner = () => {

        var that = this;

        if (Platform.OS === 'android') {
            async function requestCameraPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.CAMERA, {
                            'title': 'Camera App Permission',
                            'message': 'Camera App needs access to your camera '
                        }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {

                        that.setState({ QR_Code_Value: '' });
                        that.setState({ Start_Scanner: true });
                    } else {
                        alert("CAMERA permission denied");
                    }
                } catch (err) {
                    alert("Camera permission err", err);
                    console.warn(err);
                }
            }
            requestCameraPermission();
        } else {
            that.setState({ QR_Code_Value: '' });
            that.setState({ Start_Scanner: true });
        }
    }

    render() {

        return (
            <View style={styles.MainContainer}>
                <AppHeader
                    headerTitle={'Send Money'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                    color={'grrey'}
                />
 <View >
                    <View style={{ paddingHorizontal: 20 }}>
                        <View style={styles.Search_View}>
                            <View>
                                <Image style={styles.Search_img}
                                    source={require("../../components/icon/search1.png")} />
                            </View>
                            <View style={{ width: width - 70 }}>
                                <TextInput style={{ marginLeft: 10 }} placeholder='Search name or number'
                                    placeholderTextColor='grey' />
                            </View>
                        </View>
                    </View>
                    <View style={styles.Main_View}>
                        <View style={{ paddingHorizontal: 20, marginTop: 5 }}>
                            <View>
                                <Text style={styles.Recent_text}>Recents</Text>
                            </View>
                            <View>
                                <FlatList
                                    data={data}
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                    renderItem={({ item }) =>
                                        <View >
                                            <TouchableOpacity>
                                                <View style={{ alignItems: 'center' }}>
                                                    <Image style={{ height: 50, width: 50 }}
                                                        source={item.imgage} />
                                                    <Text style={{ fontSize: RFValue(10) }}>{item.bankname}</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    } />
                            </View>

                            <TouchableOpacity onPress={() => Actions.ScannerReader()}>
                                <LinearGradient start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    colors={['#3E1EDD', '#5C48C5']}
                                    style={styles.gradient}>
                                    <Text style={{ color: '#fff', fontSize: RFValue(20) }}>Scan QR Code</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                {/* <View style={{ backgroundColor: '#646054' }}>
                    <CameraKitCameraScreen
                        showFrame={true}
                        scanBarcode={true}
                        laserColor={'#FF3D00'}
                        frameColor={'#52B6EC'}
                        colorForScannerFrame={'#52B6EC'}
                        onReadCode={event =>
                            this.onQR_Code_Scan_Done(event.nativeEvent.codeStringValue)
                        }
                    />
                </View> */}
            </View>
        );
    }
}


export default QrScan;
