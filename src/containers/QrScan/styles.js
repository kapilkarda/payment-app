import { StyleSheet,Dimensions } from 'react-native';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export const COLOR = {
  DARK: "#040207",
  PANTOME: '#ff6f61',
  LIGHT: "#ffffff",
  BLACK: "#000",
  GRAY: "#9A9A9A",
  LIGHT_GRAY: "#ffffff",
  DANGER: "#FF5370",
  RED: "#800000",
  WHITE: "#FFF",
  CYAN: "#09818F",
  LIGHT_ORANGE: "#ff944d"
};

export default StyleSheet.create({
  MainContainer: {
    backgroundColor: "#F3F3F3",
    flex: 1
  },
  Search_View: {
    flexDirection: 'row',
    borderColor: '#000',
    borderBottomWidth: 2,
    borderBottomColor: '#3F20DD80',
    height: 40,
    alignItems: 'center',
  },
  Search_img: {
    height: 15,
    width: 15,
    marginLeft: 5,
    tintColor: '#391EBB'
  },
  Main_View: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
    marginTop: 10,
    height: height
  },
  Recent_text:{
    color: '#5C5C5C',
     fontSize: RFValue(15),
      fontWeight: 'bold'
  },
  gradient:{
    alignSelf: 'center',
    marginTop: 20,
    width: width / 2, height: 40,
    alignItems: 'center',
    justifyContent: 'center', 
    borderRadius: 20
  }

});