import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
    Platform, KeyboardAvoidingView, ScrollView,BackHandler,Alert
} from "react-native";
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';
 import AsyncStorage from '@react-native-community/async-storage';
 var currentpassword = ''
 var newpassword = ''
 var confirm_password = ''
class ChangePassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            currentpassword: '',
             newpassword: '',
              confirm_password: '',
              Authoriz:''
        };
    };
   
   async componentDidMount(){
        const user_id = await AsyncStorage.getItem('user_id')
        const Authoriz = await AsyncStorage.getItem('Acess_token')
        console.log(user_id,"user_id")
        this.setState({
            user_id:user_id,
            Authoriz:Authoriz

        })
        // BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
    }
    handlePressLogin = () => {
        this.ChangePassword()
    }
    ChangePassword() {
        if (this.state.currentpassword.length == 6 || this.state.currentpassword.length >= 6) {
            currentpassword = 'Atleast 6 characters long.'
        } else {
            Alert.alert("'Current Password should 6 digits long'")
            return false;
        }
        if (this.state.newpassword.length == 6 || this.state.newpassword.length >= 6) {
            newpassword = 'Atleast 6 characters long.'
        } else {
            Alert.alert("'New Password should 6 digits long'")
            return false;
        }
        if (this.state.confirm_password.length == 6 || this.state.confirm_password.length >= 6) {
            confirm_password = 'Atleast 6 characters long.'
        } else {
            Alert.alert("'Confirm Password should 6 digits long'")
            return false;
        }
        this.ResetPassword()
    }
    ResetPassword = () => {
        console.log(this.state.user_id,"user_id")
        this.setState({ isLoading: true })
        fetch('http://jokingfriend.com/Dero/index.php/Api/change_password', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authoriz':this.state.Authoriz
            },
            body: JSON.stringify({
                "user_id" :this.state.user_id,
                "current_password": this.state.currentpassword,
                "new_password": this.state.newpassword,
                "confirm_password": this.state.confirm_password
            })
        })
            .then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === true || res.result === "true") {

                    alert(res.msg)
                    Actions.PasswordChange()
                }
                // else{
                //     alert("Password Not Change")
                // }
            })
            .catch((e) => {
                console.log('Error BeauticianListAPI')
                console.warn(e);
            });
    };
    render() {
        var padding
        Platform.select({
            ios: () => {
                padding = 'padding'
            },
            android: () => {
                padding = 0
            }
        })();
        return (
            <KeyboardAvoidingView
                behavior={padding}
                enabled style={{ flex: 1 }}>
                <View style={styles.MainContainer}>
                    <AppHeader
                        headerTitle={'Change Password'}
                        color={'#000'}
                        backgo={require('./../../components/icon/left-arrow.png')}
                        onPress={() => this.props.navigation.goBack()} />

                    <ScrollView style={{ flex: 1, }}>
                        <View style={styles.View1}>
                            <View style={{ marginTop: '20%', paddingHorizontal: 20 }}>
                                <Text style={styles.text}>Current Password</Text>
                                <View style={styles.Viewinput}>
                                    <TextInput placeholder='Enter Current Password'
                                        placeholderTextColor='grey'
                                        secureTextEntry={true}
                                        returnKeyType="next"
                                        onChangeText={(text) => this.setState({ currentpassword: text })}
                                        value={this.state.currentpassword} />
                                </View>
                            </View>
                            <View style={{ marginTop: '10%', paddingHorizontal: 20 }}>
                                <Text style={styles.text}>New Password</Text>
                                <View style={styles.Viewinput}>
                                    <TextInput placeholder='Enter New Password'
                                        placeholderTextColor='grey'
                                        secureTextEntry={true}
                                        returnKeyType='next'
                                        onChangeText={(text) => this.setState({ newpassword: text })}
                                        value={this.state.newpassword} />
                                </View>
                                <View style={[styles.Viewinput, { marginTop: 20 }]}>
                                    <TextInput placeholder='Repeat Password'
                                        placeholderTextColor='grey'
                                        secureTextEntry={true}
                                        returnKeyType='done'
                                        onChangeText={(text) => this.setState({ confirm_password: text })}
                                        value={this.state.confirm_password}
                                        onSubmitEditing={this.handlePressLogin} />
                                </View>
                            </View>
                            <TouchableOpacity onPress={() => this.ChangePassword()}>
                                <View style={{ paddingHorizontal: 20 }}>
                                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                                        colors={['#3E1EDD', '#5C48C5']}
                                        style={styles.gradient}>
                                        <Text style={styles.text2}>SAVE PASSWORD</Text>
                                    </LinearGradient>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 100 }} />
                    </ScrollView>
                </View>
            </KeyboardAvoidingView>
        );
    }
}
export default ChangePassword;
