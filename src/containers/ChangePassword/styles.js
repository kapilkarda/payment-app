import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    View1: {
        backgroundColor: '#fff',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        marginTop: 20,
        height: '100%'
    },
    text: {
        color: '#000',
        fontSize: RFValue(20),
        fontWeight: "500"
    },
    text2: {
        color: '#fff',
        fontSize: RFValue(20),
        fontWeight: 'bold'
    },
    Viewinput: {
        borderColor: '#EFEFEF',
        borderWidth: 1,
        height: 50,
        paddingHorizontal: 10,
        borderRadius: 5,
        marginTop: 10
    },
    gradient: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#391EBB',
        marginTop: 80,
        borderRadius: 40
    }

});