import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    Main_View: {
        backgroundColor: '#fff',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        marginTop: 20,
        height: height,
        flex: 1,
    },
    Logo_View: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '20%'
    },
    Logo_img: {
        height: width / 2 + 50,
        width: width / 2 + 80
    },
    View_failed: {
        width: width / 2 + 50,
        alignSelf: 'center',
        alignItems: 'center',
    },
    tochable: {
        flex: 1,
        position: 'absolute',
        bottom: 60,
        alignSelf: 'center'
    },
    gradient: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        backgroundColor: '#391EBB',
        width: width - 50,
        borderRadius: 30
    },
    gradient_text: {
        color: '#fff',
        fontSize: RFValue(20),
        fontWeight: '600'
    }





});
