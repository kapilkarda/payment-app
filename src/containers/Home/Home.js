import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity,
    FlatList, ScrollView, BackHandler
} from "react-native";
import { Footer, FooterTab, Button, Card } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import Spinner from '../../components/Loader';
import AsyncStorage from '@react-native-community/async-storage';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import styles from './styles'
const data = [
    {
        imgage: require("../../components/icon/home13.png"),
        text1: 'Denny Boo',
        text2: 'Payment received',
        ammount: '+$ 5250 '
    },
    {
        imgage: require("../../components/icon/home13.png"),
        text1: 'Royan Krystin',
        text2: 'Money sent',
        ammount: '-$ 100 '
    },
    {
        imgage: require("../../components/icon/home13.png"),
        text1: '650XXXXX33',
        text2: 'Money sent',
        ammount: '+$ 1250 '
    },
]
class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            amount: '',
            temparray: '',
            owner_name: '',
            account_type: '',
            availableBalance: '',
            isLoading: false,
            listdata: []
        };
    };
    async componentWillMount() {
        const user_id = await AsyncStorage.getItem('user_id')
        const Authoriz = await AsyncStorage.getItem('Acess_token')
        console.log(user_id, "user_id")
        console.log(Authoriz, "Authoriz")
        this.RecentTransaction(user_id,Authoriz)

    }
    RecentTransaction = (user_id,Authoriz) => {
        this.setState({ isLoading: true })
        fetch('http://jokingfriend.com/Dero/index.php/Api/UserDashboard', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authoriz':Authoriz
            },
            body: JSON.stringify({
                "user_id": user_id
            })
        })
            .then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === true || res.result === "true") {
                    this.setState({
                        owner_name: res.data.Account.owner_name,
                        account_type: res.data.Account.account_type,
                        availableBalance: res.data.Account.balance,
                        temparray: res.data.Recent_transaction,
                        listdata: res.data.length,
                        isLoading: false
                    })
                }
                else {
                    // alert(res.msg)
                    this.setState({
                        isLoading: false
                    })
                }
            })
            .catch((e) => {
                console.log('Oops')
                console.warn(e);
            });
    };
    render() {
        return (
            <View style={styles.MainContainer}>
                <AppHeader
                    headerTitle={'Dashboard'}
                    color={'#391EBB'}
                    userprofile={require('./../../components/icon/alarm.png')} />
                <ScrollView
                    showsVerticalScrollIndicator={false} >
                    <View style={styles.View1}>
                        <View style={styles.View2}>
                            <View>
                                <Text style={styles.text2}>Owner:</Text>
                                <Text style={styles.text}> {this.state.owner_name}</Text>
                            </View>
                            <View>
                                <Text style={styles.text2}>Account Type:</Text>
                                <Text style={styles.text}> {this.state.account_type} </Text>
                            </View>
                        </View>



                        <View style={styles.View3} >
                            <View style={{ flexDirection: 'row' }} >
                                <Text style={styles.text}>Available</Text>
                                <Text style={styles.text2}>Balance</Text>
                            </View>
                            <Text>
                                <Text style={styles.text}>$</Text>
                                <Text style={[styles.text, { fontSize: RFValue(30) }]}>{this.state.availableBalance}</Text>
                            </Text>
                        </View>


                        <View style={styles.View4}>

                            <TouchableOpacity onPress={() => Actions.SendMoney()}>
                                <View style={styles.TochableView}>
                                    <Image style={styles.image1}
                                        source={require("../../components/icon/uparrow.png")} />
                                    <Text style={styles.text4}>Send</Text>

                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Actions.RequestMoneyList()}>
                                <View style={styles.TochableView2}>
                                    <Image style={styles.image1}
                                        source={require("../../components/icon/downarrow.png")} />
                                    <Text style={[styles.text4, { marginLeft: 5 }]}>Request</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={{ marginTop: 50, marginLeft: 32 }} >
                            <Text style={styles.recentText}>Recent Transaction</Text>
                        </View>
                        {this.state.listdata == 0 &&
                            <View
                                style={{ alignItems: 'center', marginVertical: 100, flex: 1, backgroundColor: "#fff", }}
                            >
                                <Text style={{
                                    fontSize: 16,
                                    color: 'grey',
                                    fontWeight: 'bold'
                                }}>No Recent Transaction</Text>
                            </View>
                        }
                        <View style={{ flex: 1, marginTop: 10 }}>
                            <FlatList
                                data={this.state.temparray}
                                renderItem={({ item }) =>
                                    <View style={{ paddingHorizontal: 28, marginTop: 10 }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View>
                                                    <Image style={{ height: 50, width: 50, }}
                                                        source={require("../../components/icon/home13.png")} />
                                                </View>
                                                <View style={{ justifyContent: 'center', marginLeft: 10 }}>
                                                    <Text style={{ fontSize: RFValue(15), color: '#000' }}>{item.receiver_name}</Text>
                                                    <Text style={{ fontSize: RFValue(12), color: 'grey' }}>{item.message}</Text>
                                                </View>
                                            </View>
                                            <View style={{ justifyContent: 'center' }}>
                                                <Text style={{ fontSize: RFValue(15), color: 'red' }}>{item.amount}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.ViewLine} />
                                    </View>}
                            />
                        </View>
                    </View>
                    <View style={{ height: 90, backgroundColor: '#f3f3f3' }}/>
                </ScrollView>
               
                {this.state.isLoading &&
                    <Spinner />
                }
            </View>
        );
    }
}


export default Home;
