import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    View1: {
        backgroundColor: '#fff', marginTop: 15, borderTopLeftRadius: 15, borderTopRightRadius: 15, minHeight: '100%'
    },
    View2: {
        flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15, marginTop: 10
    },
    View3: {
        backgroundColor: "#fff",
        alignItems: 'center',
        width: width - 35,
        alignSelf: 'center',
        padding: 30,
        marginTop: 20,
        borderRadius: 5,
        shadowColor: "#667B98",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 20,
    },
    View4: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 33,
    },
    TochableView: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        width: width / 3 + 20,
        justifyContent: 'center',
        height: 40,
        alignItems: 'center',
        borderRadius: 20,
        shadowColor: "#667B98",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 20,
    },
    TochableView2: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        width: width / 3 + 20,
        justifyContent: 'center',
        height: 40,
        alignItems: 'center',
        borderRadius: 20,
        shadowColor: "#667B98",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 20,
    },
    text: {
        color: '#391EBB',
        fontSize: RFValue(15)
    },
    text2: {
        color: '#6D6D6D',
        fontSize: RFValue(15),
        marginLeft: 5
    },
    text3: {
        color: '#000',
        fontSize: RFValue(15)
    },
    text4: {
        color: '#391EBB',
        fontSize: RFValue(17),
        fontWeight: 'bold',
        marginLeft: 4
    },
    recentText: {
        color: '#8F8F8F',
        fontSize: RFValue(17),
        fontWeight: 'bold'
    },
    image1: {
        height: 10,
        width: 10,
        tintColor: '#391EBB'
    },
    ViewLine: {
        height: 1,
        width: width - 60,
        backgroundColor: '#f3f3f3',
        alignSelf: 'center',
        marginTop: 10
    },
    ViewMainfooter:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center', 
        height: 120
    },
    ViewFooter:{
        width: '100%',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        backgroundColor: "#F3F3F3",

        position: 'absolute',
        bottom: 0
    },
    Footer:{
        backgroundColor: "#F3F3F3", 
        borderTopWidth: 0,
         height: width / 5,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15
    },
    FooterTab:{
        backgroundColor: "#fff", 
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15
    },
    Homeimg:{
        width: 37, 
        height: 35, 
        marginTop: 5 
    },
    transaction_img:{
        width: 38, height: 35, marginTop: 5
    },
    req_img:{
        width: 36, height: 35, marginTop: 5
    },
    Account_img:{
        width: 36, height: 35, marginTop: 5
    }

});
