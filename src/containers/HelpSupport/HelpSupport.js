import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, FlatList, ScrollView, BackHandler
} from "react-native";
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles from './styles'
import AppHeader from '../../components/AppHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
class HelpSupport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: '', dialogclose: false,
        };
    };
    dialogopen() {
        this.setState({ dialogclose: true })
    }
    dialogclosebutton() {
        this.setState({ dialogclose: false })
        this.setState({ dialogclose1: false })
    }

    render() {
        return (
            <View style={styles.MainContainer}>
                <AppHeader
                    headerTitle={'Help And Support'}
                    color={"#000"}
                    tintColor={'#3F20DD'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                />
                <ScrollView>
                    <View style={{ backgroundColor: '#F3F3F3', marginTop: 20 }}>
                        <View style={styles.View1}>
                            <View style={styles.View2}>
                                <Text style={styles.text}>
                                    You sent money to someone. The money left your bank account ,but..
</Text>
                            </View>
                            <View style={styles.View2}>
                                <Text style={styles.text2}>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                     Lorem Ipsum has been the industry's standard dummy text ever since
     </Text>
                            </View>
                            <TouchableOpacity>
                                <View style={{ paddingHorizontal: 20, marginTop: 15 }}>
                                    <View style={styles.TochableMore}>
                                        <Text style={{ fontSize: RFValue(15), color: '#391EBB' }}>Get More Help</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>

                            <View style={styles.View3}>
                                <View style={{ width: width - 60, alignSelf: 'center' }}>
                                    <Text style={{ color: '#000', fontSize: RFValue(17), fontWeight: '500' }}>
                                        You sent money to someone. The money left your bank account ,but..
 </Text>
                                </View>
                                <View style={styles.View2}>
                                    <Text style={{ color: 'grey', fontSize: RFValue(17), fontWeight: '500' }}>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                         Lorem Ipsum has been the industry's standard dummy text ever since
      </Text>
                                </View>
                                <TouchableOpacity>
                                    <View style={{ paddingHorizontal: 20, marginTop: 15 }}>
                                        <View style={styles.TochableMore}>
                                            <Text style={{ fontSize: RFValue(15), color: '#391EBB' }}>Get More Help</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.View4}>
                                <TouchableOpacity>
                                    <View style={styles.Tochable1}>
                                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: RFValue(15) }}>Help Articles</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.dialogopen()}>
                                    <View style={styles.Tochable2}>
                                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: RFValue(15) }}>Contact</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={{ backgroundColor: '#fff', height: 80 }}></View>
                </ScrollView>
                <Dialog
                    visible={this.state.dialogclose}
                    onTouchOutside={() => {
                        this.setState({ dialogclose: false });
                    }}>
                    <DialogContent
                        style={{
                            width: width / 2 + 60,
                            height: 170,
                            //paddingTop: 10
                            backgroundColor: '#fff'
                        }}>
                        <View style={{ alignItems: 'center', marginTop: 10 }}>
                            <View>
                                <Text style={{ color: '#626262', fontSize: RFValue(25) }}>Call</Text>
                            </View>
                            <View>
                                <Text style={{ color: '#626262', fontSize: RFValue(25) }}>159 384 372</Text>
                            </View>
                            <View>
                                <Text style={{ color: '#626262', fontSize: RFValue(15) }}>Are you sure you want to call</Text>
                            </View>
                            <View style={{ flexDirection: 'row', width: width / 2 + 30, justifyContent: 'space-around', alignItems: 'center' }}>
                                <TouchableOpacity>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                        <Image style={{ height: hp('10%'), width: wp('20%') }}
                                            source={require('../../components/icon/dialog1.png')} />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <View style={{ justifyContent: 'center' }}>
                                        <Image style={{ height: 47, width: 47 }}
                                            source={require('../../components/icon/dialog2.png')} />
                                    </View>
                                </TouchableOpacity>
                            </View>



                        </View>

                    </DialogContent>
                </Dialog>
            </View>


        );
    }
}


export default HelpSupport;
