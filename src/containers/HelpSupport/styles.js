import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    View1: {
        backgroundColor: '#fff',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    View2: {
        width: width - 60,
        alignSelf: 'center',
        marginTop: 20
    },
    View3: {
        backgroundColor: '#fff',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        marginTop: 30
    },
    View4: {
        flexDirection: 'row',
        marginTop: 20,
        justifyContent: 'space-around',
        width: width - 10,
        alignSelf: 'center',
        marginTop: 30
    },

    text: {
        color: '#000',
        fontSize: RFValue(17),
        fontWeight: '500'
    },
    text2: {
        color: 'grey',
        fontSize: RFValue(17),
        fontWeight: '500'
    },
    TochableMore: {
        borderColor: '#391EBB',
        borderWidth: 1,
        width: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        borderRadius: 20
    },
    Tochable1: {
        width: width / 4 + 40,
        height: 40,
        backgroundColor: '#391EBb',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        marginRight: 30
    },
    Tochable2: {
        width: width / 4 + 30,
        height: 40,
        backgroundColor: '#391EBb',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20

    }
});
