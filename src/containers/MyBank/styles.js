import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    View1: {
        marginTop: 40,
        backgroundColor: '#fff',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        minHeight: '40%'
    },
    View2: {
        marginLeft: 25,
        marginTop: 10
    },
    LineView: {
        height: 1,
        backgroundColor: '#f3f3f3',
        width: '95%',
        alignSelf: 'center',
        marginTop: 5
    },
    ViewList: {
        minHeight: width / 2 - 20,
        marginTop: 10
    },
    View3: {
        flexDirection: 'row',
        width: width / 2 - 40,
        justifyContent: 'space-between'
    },
    text: {
        color: '#000',
        fontSize: RFValue(20)
    },
    footer_main: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center', height: 120
    },
    footer_view: {
        width: '100%',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        backgroundColor: "#F3F3F3",
        //   justifyContent: 'center', 
        //   alignItems: 'center',
        position: 'absolute',
        bottom: 0
    },
    footer_View1: {
        backgroundColor: "#F3F3F3",
        borderTopWidth: 0,
        height: width / 5,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15
    },
    footer_Tab: {
        backgroundColor: "#fff",
         borderRadius: 15,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15
    },
    Home_button:{
        width: 45,
         height: 44, 
         marginTop: 5
    },
    Trans_image:{
        width: 38, height: 35, marginTop: 5 
    },
    Req_img:{
        width: 36, height: 35, marginTop: 5
    },
    user_profile:{
        width: 32, height: 32, marginTop: 8
    }




});
