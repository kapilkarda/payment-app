import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, FlatList, ScrollView, BackHandler
} from "react-native";
import styles from './styles'
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const data = [
    {
        imgage: require("../../components/icon/addbank1.png"),
        bankname: 'Swedbank',
        accountno: 'XXXXXX45677'

    },
    {
        imgage: require("../../components/icon/addbank1.png"),
        bankname: 'Swedbank',
        accountno: 'XXXXXX45677'
    },
    {
        imgage: require("../../components/icon/addbank1.png"),
        bankname: 'Swedbank',
        accountno: 'XXXXXX45677'
    },
]
class MyBank extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: ''
        };
    };

    render() {
        return (
            <View style={styles.MainContainer}>
                <AppHeader
                    headerTitle={'Add Bank Account'}
                    tintColor={'#391EBB'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                />

                <ScrollView>
                    <View style={styles.View1}>
                        <View style={styles.View2} >
                            <Text style={styles.text}>MyBank</Text>
                        </View>
                        <View style={styles.ViewList}>
                            <FlatList
                                data={data}
                                renderItem={({ item }) =>
                                    <View style={{ marginTop: 5, paddingHorizontal: 20, }}>
                                        <View style={styles.View3}>
                                            <View>
                                                <Image style={{ height: 50, width: 50 }} source={item.imgage} />
                                            </View>
                                            <View style={{ justifyContent: 'center' }}>
                                                <Text style={{ color: '#000', fontSize: RFValue(15) }}>{item.bankname}</Text>
                                                <Text style={{ color: 'grey', fontSize: RFValue(12) }}>{item.accountno}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.LineView}></View>
                                    </View>
                                } />
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => Actions.AddBank()}>
                        <View style={{ alignSelf: 'flex-end', width: 80, marginTop: 15 }}>
                            <Image style={{ height: 50, width: 50 }} 
                            source={require('../../components/icon/addbank2.png')} />
                        </View>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        );
    }
}
export default MyBank;
