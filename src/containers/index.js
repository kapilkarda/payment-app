import React, { Component } from 'react';
import { Scene, Router, Actions, Reducer, ActionConst, Overlay, Tabs, Modal, Drawer, Stack, Lightbox, } from "react-native-router-flux";
import { connect } from "react-redux";
import { Title } from "native-base";
import { StyleSheet, Text, TouchableOpacity, Image, View, Dimensions } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import Loader from './../constants/Loader';


import Home from './../containers/Home';
import Request from './../containers/Request';
import AddAccount from './../containers/AddAccount';
import AddBank from './../containers/AddBank';
import MyBank from './../containers/MyBank';
import UserProfile from './../containers/UserProfile'
import Transaction from './../containers/Transaction'
import SendMoney from './SendMoney/SendMoney';
import SignIn from './../containers/SignIn'
import RegisterNumber from './../containers/RegisterNumber'
import LoginBankId from './../containers/LoginBankId'
import LoginPassword from './../containers/LoginPassword'
import PersonalDetail from './../containers/PersonalDetail'
import HelpSupport from './../containers/HelpSupport'
import VerifyNo from './../containers/VerifyNo'
import AppIntro from './../containers/AppIntro'
import ChangePassword from './../containers/ChangePassword'
import PasswordChange from './../containers/PasswordChange'
import RequestMoney from './../containers/RequestMoney'
import Notification from './../containers/Notification'
import NotificatioError from './../containers/NotificatioError'
import PaymentFailed from './../containers/PaymentFailed'
import RequstSucessfull from './../containers/RequstSucessfull'
import PaymentSucess from './../containers/PaymentSucess'
import RequestMoneyList from './../containers/RequestMoneyList'
import QrScan from './../containers/QrScan'
import PaymentSend from './../containers/PaymentSend'
import BottomNavigation1 from './../containers/BottomNavigation1'
import ReqFailed from './../containers/ReqFailed'
import ScannerReader from './../containers/ScannerReader'
import QrShare from './../containers/QrShare'
import TearmsService from './../containers/TearmsService'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full width
var image;
const TabIcon = ({ selected, title, img, focused }) => {
    switch (title) {
        case 'Home':
            image = focused ? require('../components/icon/footerimg/home1.png') : require('../components/icon/footerimg/home.png');
            break;

        case 'Transaction':
            image = focused ? require('../components/icon/footerimg/Transaction1.png') : require('../components/icon/footerimg/Transaction.png');
            break;
        case 'Request':
            image = focused ? require('../components/icon/footerimg/Request1.png') : require('../components/icon/footerimg/Request.png');
            break;
        case 'UserProfile':
            image = focused ? require('../components/icon/footerimg/account.png') : require('../components/icon/footerimg/account1.png');
            break;

    }
    return (
        <Image source={image}
            style={{ width: 35, height: 35, }}>
        </Image>
        // <Text style={{ color: selected ? 'red' : "black" }}>{title}</Text>
    )
}
var width = Dimensions.get('window').width; //full width
const RouterWithRedux = connect()(Router);
class Root extends Component {
    constructor(props) {
        super(props);

        this.state = {
            token: null,
            isStorageLoaded: false,
            apintroState: false,
            homeState: false

        }
    }


    async componentWillMount() {
        const user_id = await AsyncStorage.getItem('user_id')
        console.log(user_id, "user_id")
        try {
            if (user_id !== null && user_id !== "" && user_id !== undefined) {
                this.setState({
                    homeState: true
                })
            } else {
                this.setState({
                    apintroState: true
                })
            }
        }
        catch (error) {
            console.log('error' + error)
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('token').then((token) => {
            this.setState({
                token: token !== null,
                isStorageLoaded: true
            })
        });
    }


    render() {
        let { isStorageLoaded } = this.state;
        if (!isStorageLoaded) {
            return (
                <Loader loading={true} />
            )
        } else {
            return (
                <RouterWithRedux navigationBarStyle={{ backgroundColor: '#5dca67', }}>
                    <Scene key="root" hideNavBar hideTabBar>
                        <Stack key="app">
                            <Scene
                                component={AppIntro}
                                initial={this.state.apintroState}
                                hideNavBar={true}
                                key='AppIntro'
                                title='AppIntro'
                            />
                            <Scene
                                key='tabbar'
                                initial={this.state.homeState}
                                tabs
                                hideNavBar={true}
                                tabBarStyle={{
                                    backgroundColor: '#FFFFFF',
                                    borderTopLeftRadius: 15,
                                    borderTopRightRadius: 15,
                                    borderTopWidth: 0, height: 60
                                }}
                            >
                                <Scene key="Homes" title="Home" icon={TabIcon} img={image}>
                                    <Scene
                                        component={Home}
                                        hideNavBar={true}
                                        key='Home'
                                        title='Home'
                                    />
                                </Scene>
                                <Scene key="Transactions" title="Transaction" icon={TabIcon} img={image}>
                                    <Scene
                                        component={Transaction}
                                        initial={false}
                                        hideNavBar={true}
                                        key='Transaction'
                                        title='Transaction'
                                    />
                                </Scene>
                                <Scene key="Requests" title="Request" icon={TabIcon} img={image}>
                                    <Scene
                                        component={Request}
                                        initial={false}
                                        key='Request'
                                        title='Request'
                                        hideNavBar={true}
                                    />
                                </Scene>
                                <Scene key="UserPro" title="UserProfile" icon={TabIcon} img={image}>
                                    <Scene
                                        component={UserProfile}
                                        initial={false}
                                        hideNavBar={true}
                                        key='UserProfile'
                                        title='UserProfile'
                                    />
                                    <Scene
                                        component={MyBank}
                                        initial={false}
                                        hideNavBar={true}
                                        key='MyBank'
                                        title='MyBank'
                                    />
                                    <Scene
                                        component={AddBank}
                                        initial={false}
                                        hideNavBar={true}
                                        key='AddBank'
                                        title='AddBank'
                                    />
                                    <Scene
                                        component={HelpSupport}
                                        initial={false}
                                        hideNavBar={true}
                                        key='HelpSupport'
                                        title='HelpSupport'
                                    />
                                    <Scene
                                        component={TearmsService}
                                        initial={false}
                                        hideNavBar={true}
                                        key='TearmsService'
                                        title='TearmsService'
                                    />

                                </Scene>
                            </Scene>

                            <Scene
                                component={AddAccount}
                                initial={false}
                                hideNavBar={true}
                                key='AddAccount'
                                title='AddAccount'
                            />

                            <Scene
                                component={SendMoney}
                                initial={false}
                                hideNavBar={true}
                                key='SendMoney'
                                title='SendMoney'
                            />
                            <Scene
                                component={SignIn}
                                initial={false}
                                hideNavBar={true}
                                key='SignIn'
                                title='SignIn'
                            />
                            <Scene
                                component={RegisterNumber}
                                initial={false}
                                hideNavBar={true}
                                key='RegisterNumber'
                                title='RegisterNumber'
                            />
                            <Scene
                                component={LoginBankId}
                                initial={false}
                                hideNavBar={true}
                                key='LoginBankId'
                                title='LoginBankId'
                            />
                            <Scene
                                component={LoginPassword}
                                initial={false}
                                hideNavBar={true}
                                key='LoginPassword'
                                title='LoginPassword'
                            />
                            <Scene
                                component={PersonalDetail}
                                initial={false}
                                hideNavBar={true}
                                key='PersonalDetail'
                                title='PersonalDetail'
                            />

                            <Scene
                                component={VerifyNo}
                                initial={false}
                                hideNavBar={true}
                                key='VerifyNo'
                                title='VerifyNo'
                            />

                            <Scene
                                component={ChangePassword}
                                initial={false}
                                hideNavBar={true}
                                key='ChangePassword'
                                title='ChangePassword'
                            />
                            <Scene
                                component={PasswordChange}
                                initial={false}
                                hideNavBar={true}
                                key='PasswordChange'
                                title='PasswordChange'
                            />
                            <Scene
                                component={RequestMoney}
                                initial={false}
                                hideNavBar={true}
                                key='RequestMoney'
                                title='RequestMoney'
                            />

                            <Scene
                                component={Notification}
                                initial={false}
                                hideNavBar={true}
                                key='Notification'
                                title='Notification'
                            />
                            <Scene
                                component={NotificatioError}
                                initial={false}
                                hideNavBar={true}
                                key='NotificatioError'
                                title='NotificatioError'
                            />

                            <Scene
                                component={PaymentFailed}
                                initial={false}
                                hideNavBar={true}
                                key='PaymentFailed'
                                title='PaymentFailed'
                            />
                            <Scene
                                component={RequstSucessfull}
                                initial={false}
                                hideNavBar={true}
                                key='RequstSucessfull'
                                title='RequstSucessfull'
                            />
                            <Scene
                                component={PaymentSucess}
                                initial={false}
                                hideNavBar={true}
                                key='PaymentSucess'
                                title='PaymentSucess'
                            />
                            <Scene
                                component={RequestMoneyList}
                                initial={false}
                                hideNavBar={true}
                                key='RequestMoneyList'
                                title='RequestMoneyList'
                            />
                            <Scene
                                component={QrScan}
                                initial={false}
                                hideNavBar={true}
                                key='QrScan'
                                title='QrScan'
                            />
                            <Scene
                                component={PaymentSend}
                                initial={false}
                                hideNavBar={true}
                                key='PaymentSend'
                                title='PaymentSend'
                            />
                            <Scene
                                component={BottomNavigation1}
                                initial={false}
                                hideNavBar={true}
                                key='BottomNavigation1'
                                title='BottomNavigation1'
                            />
                            <Scene
                                component={ReqFailed}
                                initial={false}
                                hideNavBar={true}
                                key='ReqFailed'
                                title='ReqFailed'
                            />
                            <Scene
                                component={ScannerReader}
                                initial={false}
                                hideNavBar={true}
                                key='ScannerReader'
                                title='ScannerReader'
                            />
                            <Scene
                                component={QrShare}
                                initial={false}
                                hideNavBar={true}
                                key='QrShare'
                                title='QrShare'
                            />


                        </Stack>
                    </Scene>
                </RouterWithRedux >

            )
        }
    }
}
const mapStateToProps = (state) => {
    return {
        login: state.login
    }
};
export default connect(mapStateToProps)(Root)
console.disableYellowBox = true