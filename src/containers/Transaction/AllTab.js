import React from "react";
import {
    View, Text, StatusBar,
    Image, Dimensions, ImageBackground, TouchableOpacity, TextInput, ScrollView,
    ListView, FlatList
} from "react-native";
import { Actions } from 'react-native-router-flux';
import { Footer, FooterTab, Button } from "native-base";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import RF, { RFValue } from "react-native-responsive-fontsize";
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from '../../components/Loader';
class AllTab extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            user_id: '',
            listdata: [],
            tempArray: [],
            isLoading: false
        };
    };
    async componentWillMount() {
        const user_id = await AsyncStorage.getItem('user_id')
        const Authoriz = await AsyncStorage.getItem('Acess_token')
        console.log(user_id, "user_id")
        this.AllTransaction(user_id,Authoriz)
    }
    AllTransaction = (user_id,Authoriz) => {
        this.setState({ isLoading: true })
        fetch('http://jokingfriend.com/Dero/index.php/Api/transactionHistory', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authoriz':Authoriz
            },
            body: JSON.stringify({
                "sender_id": user_id
            })
        })
            .then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === true || res.result === "true") {
                    this.setState({
                        listdata: res.data.All,
                        tempArray: res.data.length,
                        isLoading: false
                    })
                }
                else {
                    this.setState({

                        tempArray: res.data.length,
                        isLoading: false
                    })
                }

            })
            .catch((e) => {
                console.log('Error BeauticianListAPI')
                console.warn(e);

            });
    };
    render() {
        return (

            <View style={{ backgroundColor: "#f3f3f3", flex: 1, paddingTop: 30, }}>
                <ScrollView>
                    <View style={{ flex: 1 }}>
                        {this.state.tempArray == 0 &&
                            <View
                                style={{ alignItems: 'center', marginVertical: 100, flex: 1, height: height, backgroundColor: "#f3f3f3", }}
                            >
                                <Text style={{
                                    fontSize: 16,
                                    color: 'grey',
                                    fontWeight: 'bold'
                                }}>No All Transaction</Text>
                            </View>
                        }
                        <View style={{ backgroundColor: '#fff', borderTopLeftRadius: 15, borderTopRightRadius: 15, }}>
                            {/* <View style={{ marginLeft: 30, marginTop: 20 }}>
                            <Text style={{ color: '#80808080', fontSize: RFValue(15),  fontWeight:'900' }}> July 31, 2019</Text>
                        </View> */}
                            <View style={{ marginTop: 10 }}>
                                <FlatList
                                    data={this.state.listdata}
                                    renderItem={({ item }) =>
                                        <View style={{ paddingHorizontal: 20, }}>
                                            <View style={{ marginLeft: 10, marginTop: 20 }}>
                                                <Text style={{ color: '#80808080', fontSize: RFValue(15), fontWeight: '900' }}> {item.date}</Text>
                                            </View>
                                            <View style={{ height: 10, }} />
                                            {item.tans.map((value, index) => (

                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10, borderBottomColor: '#f3f3f3', borderBottomWidth: 1 }}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <View>
                                                            <Image style={{ height: 50, width: 50 }}
                                                                source={require("../../components/icon/home13.png")} />
                                                        </View>
                                                        <View style={{ marginLeft: 10, justifyContent: 'center' }}>
                                                            <Text style={{ color: '#000', fontSize: RFValue(18) }}>{value.user_name}</Text>
                                                            <Text style={{ color: 'grey', fontSize: RFValue(15) }}>{value.phone_number}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ justifyContent: 'center' }}>
                                                        <Text style={{ color: 'red', fontSize: RFValue(12) }}>{value.amount}</Text>
                                                    </View>
                                                </View>
                                            ))}
                                            {/* <View style={{ backgroundColor: '#f3f3f3', height: 1, width: width - 50, alignSelf: 'center', marginTop: 10 }}></View> */}





                                        </View>



                                    } />
                            </View>
                            <View style={{ backgroundColor: '#fff', height: 35 }}></View>
                        </View>

                    </View>

                </ScrollView>
               
                {this.state.isLoading &&
                    <Spinner />
                }
            </View>

        );
    }
}


export default AllTab;
