import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity,
    FlatList, ScrollView
} from "react-native";
import { Footer, FooterTab, Button, } from "native-base";
import { Actions } from 'react-native-router-flux';

import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
var ScrollableTabView = require('react-native-scrollable-tab-view');
import AllTab from './AllTab';
import ReceivedTab from './ReceivedTab';
import SentTab from './SentTab';

import AppHeader from '../../components/AppHeader';

class Transaction extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: ''
        };
    };

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#f3f3f3", }}>
               <ScrollableTabView style={{backgroundColor:'#fff',
                }}
                tabBarActiveTextColor={'#3F20DD80'}
                tabBarUnderlineStyle={{backgroundColor:'#3F20DD80',marginLeft:35,width:50,	}}  
                  >
                 <AllTab tabLabel="All"  />
                   <ReceivedTab tabLabel="Received" />
                    <SentTab tabLabel="Sent" />
                </ScrollableTabView>
               
            </View>
        );
    }
}


export default Transaction;
