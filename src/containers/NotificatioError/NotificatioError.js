import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity,BackHandler
} from "react-native";
import styles from './styles'
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
class NotificatioError extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: ''
        };
    };
    // componentWillMount(){
    //     BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
    // }

    render() {
        return (
            <View style={styles.MainContainer}>
                <AppHeader
                    headerTitle={' Notifications'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                    color={"#636363"}
                />
              
                <View style={styles.View1}>
                    <View style={styles.View2}>
                        <Image style={styles.Image1} 
                        source={require('./../../components/icon/ae.png')} />
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.text}>Oops!..</Text>
                        <Text style={styles.text2}>You don't have any notification yet !</Text>
                    </View>
                </View>
            </View>
        );
    }
}
export default NotificatioError;
