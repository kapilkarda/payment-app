import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    View1: {
        backgroundColor: '#fff',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        marginTop: 20,
        height: height,
        flex: 1
    },
    View2: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    Image1: {
        height: '66%',
        width: '60%',
    },
    text: {
        color: '#391EBB',
        fontSize: RFValue(30)
    },
    text2: {
        color: '#ABABAB',
        fontSize: RFValue(18),
        marginTop: 10
    }



});
