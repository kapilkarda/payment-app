import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, ScrollView, BackHandler
} from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import Spinner from '../../components/Loader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import styles from "./styles";
class RequstSucessfull extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            Amount: '',
            Name: '',
            phone_number: '',
            result: '',
            requestSuccess: false,
            successImg: false,
            isLoading: false,
            confirmRequest: true,
            continueButton: false,
            confirmRequestButton: false,
            date: '',
            Authoriz:''
        };
    };

    async componentWillMount() {
        const user_id = await AsyncStorage.getItem('user_id')
        const Authoriz = await AsyncStorage.getItem('Acess_token')
        const Amount = this.props.Amount
        const Name = this.props.Name
        const phone_number = this.props.phone_number
        this.setState({
            Amount: Amount,
            Name: Name,
            phone_number: phone_number,
            user_id: user_id,
            Authoriz:Authoriz
        })
        // BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
    }

    RequestMoney = () => {
        this.setState({ isLoading: true })
        fetch('http://jokingfriend.com/Dero/index.php/Api/request_money', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authoriz': this.state.Authoriz
            },
            body: JSON.stringify({
                "user_id": this.state.user_id,
                "user_name": this.state.Name,
                "phone_number": this.state.phone_number,
                "pay_by_bank": "Hello",
                "bank_name": "SBI",
                "amount": this.state.Amount
            })
        })
            .then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === true || res.result === "true") {
                    this.setState({
                        date: res.created_at,
                        result: res.result,
                        requestSuccess: true,
                        successImg: true,
                        isLoading: false,
                        confirmRequest: false,
                        continueButton: true,
                        confirmRequestButton: true,
                    })
                }
                else {
                    this.setState({ isLoading: false })
                    Actions.ReqFailed()
                }
            })
            .catch((e) => {
                console.log('Oops')
                console.warn(e);
            });
    };

    nextHome = () => {
        Actions.Home();
    }

    render() {

        return (
            <View style={{ flex: 1, backgroundColor: "#F3F3F3", }}>
                <AppHeader
                    headerTitle={'Request Money'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                    color={"#636363"}
                />
                <View style={styles.Main_View}>
                    <View style={{ paddingHorizontal: 20, flex: 1 }}>
                        {
                            this.state.requestSuccess &&
                            <View style={{ marginTop: 20 }}>
                                <Text style={styles.Sucess_text}>Request Successful</Text>
                            </View>
                        }
                        <View style={styles.Amount_view}>
                            <Text style={styles.Amount_text}>$ {this.state.Amount}</Text>
                            {
                                this.state.successImg &&
                                <Image style={{ height: 30, width: 30 }}
                                    source={require("./../../components/icon/sucessful.png")} />
                            }
                        </View>

                        <View style={{ width: width / 2 + 115, marginTop: 30 }}>
                            <Text style={{ color: '#999999', fontSize: RFValue(18) }}>{this.state.Name}, linked with bank a/c  XXXXX5678</Text>
                        </View>

                        <View style={{ marginTop: 20 }}>
                            <Text style={{ color: '#000', fontSize: RFValue(25) }}>XXXX5678</Text>
                            <Text style={styles.date_text}>{this.state.date}</Text>
                        </View>
                    </View>

                    {
                        this.state.confirmRequest &&
                        <TouchableOpacity style={styles.confirm_tochable}
                            onPress={() => this.RequestMoney()}
                            disabled={this.state.confirmRequestButton}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={['#3E1EDD', '#5C48C5']}
                                style={styles.gradient} >
                                <Text style={styles.tochable_text}>PROCEED</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    }

                    {
                        this.state.continueButton &&
                        <TouchableOpacity style={styles.confirm_tochable}
                            onPress={() => this.nextHome()}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={['#3E1EDD', '#5C48C5']}
                                style={styles.gradient} >
                                <Text style={styles.tochable_text}>Continue</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    }
                    {this.state.isLoading &&
                        <Spinner />
                    }
                </View>
            </View>
        );
    }
}


export default RequstSucessfull;
