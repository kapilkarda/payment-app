import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    Main_View: {
        backgroundColor: '#fff',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        marginTop: 20,
        height: height,
        flex: 1,
    },
    Sucess_text:{
        color: '#252525',
         fontSize: RFValue(20), 
         fontWeight: '900'
    },
    Amount_view:{
        flexDirection: 'row', justifyContent: 'space-between', marginTop: 20
    },
    Amount_text:{
        fontSize: RFValue(25), color: '#391EBB', fontWeight: '900'
    },
    confirm_tochable:{
        flex: 1, position: 'absolute', bottom: 50, alignSelf: 'center' 
    },
    
    gradient:{
        backgroundColor: '#351EBB', width: width - 50, height: 50, alignItems: 'center',
        justifyContent: 'center', alignSelf: 'center', borderRadius: 30,
    },
    tochable_text:{
        color: '#fff', fontWeight: 'bold'
    },
    date_text:{
        color: '#999999', fontSize: RFValue(15), marginTop: 10
    }



});
