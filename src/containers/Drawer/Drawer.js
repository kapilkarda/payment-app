import React from 'react'
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
    ScrollView, ListView, FlatList,
} from "react-native";
import styles from './styles';
import { Actions } from "react-native-router-flux";
// import RF from "react-native-responsive-fontsize"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const items = [
    {
        img1: require("../../components/Images/image1.png"),
        name: "Gramercy Tavern",
        img2: require("../../components/Images/rightarrow.png"),
        miles: "0.5 miles",
        img3: require("../../components/Images/shoplocation.png"),
        streetname: "Osteria"
    },
    {
        img1: require("../../components/Images/image2.png"),
        name: "La Bernardin",
        img2: require("../../components/Images/rightarrow.png"),
        miles: "0.5 miles",
        img3: require("../../components/Images/shoplocation.png"),
        streetname: "Steakhouse"
    },
    {
        img1: require("../../components/Images/image3.png"),
        name: "Blue Hill",
        img2: require("../../components/Images/rightarrow.png"),
        miles: "0.7 miles",
        img3: require("../../components/Images/shoplocation.png"),
        streetname: "Restaurent Buffet"
    },
    {
        img1: require("../../components/Images/image4.png"),
        name: "Per Se",
        img2: require("../../components/Images/rightarrow.png"),
        miles: "0.8 miles",
        img3: require("../../components/Images/shoplocation.png"),
        streetname: "Steakhouse"
    },
];


class DrawerContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        };
    }

    toggleOpen = () => {
        this.setState({ open: !this.state.open });
    };

    render() {
        return (
            <TouchableOpacity onPress={this.toggleOpen} style={styles.animatedBox}>
                <View style={{flex:1}}>
                    <View style={{ alignSelf: 'center', marginTop: 20 }}>
                        <TouchableOpacity
                         onPress={() => Actions.Profile()}
                        >
                        <Image source={require("../../components/Images/Profile1.png")}
                            style={{ width: wp('35%'), height: hp("25%"), marginTop: 3 }}>
                        </Image>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity>
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            alignSelf: 'center',
                            justifyContent: 'center',
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:20,
                            paddingHorizontal:13
                        }}>
                            <View style={{flexDirection:'row'}}>
                                <Image source={require("../../components/Images/icon.png")}
                                    style={{ width: wp('3.5%'), height: hp('2.5%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Home</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            alignSelf: 'center',
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:6,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("../../components/Images/notification.png")}
                                    style={{ width: wp('3.2%'), height: hp('2.5%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Notifications</Text>
                            </View>
                            <View style={{flexDirection:'row',marginLeft:10,alignSelf:'center'}}>
                                <Image source={require("../../components/Images/notify.png")}
                                    style={{ width: wp('7%'), height: hp('4%'),}}>
                                </Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            alignSelf: 'center',
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:6,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("../../components/Images/cart.png")}
                                    style={{ width: wp('4%'), height: hp('2%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Cart</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            alignSelf: 'center',
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:6,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("../../components/Images/orders.png")}
                                    style={{ width: wp('3.2%'), height: hp('2.5%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Orders</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity 
                      onPress={() => Actions.Settings()}
                    >
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            alignSelf: 'center',
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:6,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("../../components/Images/setting.png")}
                                    style={{ width: wp('3.2%'), height: hp('2%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Settings</Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity style={{
                        position:'absolute',
                        bottom:85,
                        alignSelf: 'center',
                    }}>
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:10,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13,
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("../../components/Images/signupdrawer.png")}
                                    style={{ width: wp('4%'), height: hp('2%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Sign up as a vendor</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        position:'absolute',
                        bottom:40,
                        alignSelf: 'center',
                    }}>
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:10,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13,
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("../../components/Images/logout.png")}
                                    style={{ width: wp('4%'), height: hp('2.5%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Log Out</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                  </View>
            </TouchableOpacity>
          );
    };
}

export default DrawerContent;

