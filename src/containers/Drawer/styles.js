import { StyleSheet, Dimensions,Platform } from 'react-native'; 
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


export const COLOR = {
    BLACK: "#000",
    GRAY: "#9A9A9A",
    WHITE: "#FFF",
    LIGHT_ORANGE: "#E69151"
};
var padding
var paddingvet
    Platform.select({
      ios: () => {
        padding = 15
        paddingvet = null
      },
      android: () => {
        padding = 0
        paddingvet = 15
      }
    })();
export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    container1: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
        zIndex: 0,
      },
    view1:{
        height: 45,
        width: width - 30,
        backgroundColor: '#E6E6E6',
        borderRadius: 25,
        justifyContent: 'space-between',
        alignSelf: 'center',
        flexDirection: 'row',
        marginTop:15
    },
    touchable1:{
        flexDirection: 'row',
        padding: 10,
        width: width / 3 - 13,
        backgroundColor: '#5dca67',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25
    },
    touchable2:{
        flexDirection: 'row', 
        padding: 10, 
        width: width / 3 - 13,
        backgroundColor: '#5dca67', 
        alignItems: 'center', 
        justifyContent: 'center',
        borderRadius: 25, 
        marginLeft: 3
    },
    animatedBox: {
        flex: 1,
        backgroundColor: "#fff",
        padding: 10
      },
      

});