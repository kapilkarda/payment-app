import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, FlatList, ScrollView, TouchableOpacity
} from "react-native";
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import styles from './styles';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const data = [
    {
        imgage: require("../../components/icon/addbank1.png"),
        bankname: 'Swedbank',
    },
    {
        imgage: require("../../components/icon/addbank1.png"),
        bankname: 'ICAbanen',
    },
    {
        imgage: require("../../components/icon/addbank1.png"),
        bankname: 'Nordea',
    },
    {
        imgage: require("../../components/icon/addbank1.png"),
        bankname: 'ICAbanen',
    },
    {
        imgage: require("../../components/icon/addbank1.png"),
        bankname: 'ICAbanen',
    },
    {
        imgage: require("../../components/icon/addbank1.png"),
        bankname: 'Swedbank',
    },
    {
        imgage: require("../../components/icon/addbank1.png"),
        bankname: 'ICAbanen',
    },
]
class AddBank extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: ''
        };
    };
   
 render() {
        return (
            <View style={styles.MainContainer}>
                <AppHeader
                    headerTitle={'Add Bank Account'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()} />
                  <ScrollView style={{ height: hp('60%') }}>
                    <View style={styles.View1}>
                        <Text style={styles.text}>Choose Your Bank</Text>
                        <View style={{ marginTop: 10 }}>
                            <FlatList
                                data={data}
                                renderItem={({ item }) =>
                                    <TouchableOpacity onPress={() => Actions.AddAccount()}>
                                        <View style={{ padding: 5, marginLeft: 10 }}>
                                            <View style={{ flexDirection: 'row', }}>
                                                <View>
                                                    <Image style={{ height: 45, width: 45 }} source={item.imgage} />
                                                </View>
                                                <View style={{ justifyContent: 'center', marginLeft: 15 }}>
                                                    <Text style={{ color: '#000', fontSize: RFValue(15) }}>{item.bankname}</Text>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={styles.LineView}/>
                                    </TouchableOpacity>
                                } />
                        </View>
                    </View>
                </ScrollView>
                </View>
        );
    }
}
export default AddBank;
