import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    View1: {
        marginTop: 25,
        backgroundColor: '#fff',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    LineView: {
        height: 1,
        backgroundColor: '#f3f3f3',
        width: '90%',
        alignSelf: 'center',
    },
    text: {
        fontSize: RFValue(20),
        color: '#000',
        marginLeft: 20,
        marginTop: 10
    },
    footer_main: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 120
    },
    footer_view: {
        width: '100%',
        backgroundColor: "#F3F3F3",
        position: 'absolute',
        bottom: 0,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15
    },
    footer: {
        backgroundColor: "#F3F3F3",
        borderTopWidth: 0,
        height: width / 5,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15
    }


});