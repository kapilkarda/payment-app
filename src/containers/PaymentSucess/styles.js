import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    Main_View:{
        backgroundColor: '#fff', borderTopRightRadius: 15, borderTopLeftRadius: 15,
        marginTop: 20, height: height, flex: 1, 
    },
    Sucess_text:{
        color: '#252525', fontSize: RFValue(20), fontWeight: '900'
    },
    ammount_view:{
        flexDirection: 'row', justifyContent: 'space-between', marginTop: 20
    },
    amount_text:{
        fontSize: RFValue(25), color: '#391EBB', fontWeight: '900' 
    },
    gradient:{
        backgroundColor: '#351EBB', width: width - 50, height: 50, alignItems: 'center',
        justifyContent: 'center', alignSelf: 'center', borderRadius: 30,
    },
    tochable:{
        flex: 1, position: 'absolute', bottom: 50, alignSelf: 'center'
    },
    date_text:{
        color: '#999999', fontSize: RFValue(15), marginTop: 10
    },
    account_text:{
        color: '#000', fontSize: RFValue(25)
    },
    Dialog_View:{
        alignItems: 'center', marginTop: 10 
    },
    tranasfr_text:{
        color: '#351EBB', fontSize: RFValue(20), marginLeft: 10
    },
    tochable_view:{
        alignSelf: 'flex-end', flexDirection: 'row', marginTop: 60, justifyContent: 'space-between', width: width / 3
    }





});
