import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, ScrollView, BackHandler
} from "react-native";
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import styles from "./styles";
class PaymentSucess extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            dialogclose: false,
            AccountHolderName: ''
        };
    };
    dialogopen() {
        this.setState({ dialogclose: true })
    }
    componentWillMount() {
        const AccountHolderName = this.props.AccountHolderName
        this.setState({
            AccountHolderName: AccountHolderName
        })
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#F3F3F3", }}>
                <AppHeader
                    headerTitle={'Send Money'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                    color={"#636363"}
                />
                <View style={styles.Main_View}>
                    <View style={{ paddingHorizontal: 20, flex: 1 }}>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.Sucess_text}>Payment Successful</Text>
                        </View>
                        <View style={styles.ammount_view}>
                            <Text style={styles.amount_text}>$ 200</Text>
                            <Image style={{ height: 30, width: 30 }}
                                source={require("./../../components/icon/sucessful.png")} />
                        </View>
                        <View style={{ width: width / 2 + 115, marginTop: 30 }}>
                            <Text style={{ color: '#999999', fontSize: RFValue(18) }}>To {this.state.AccountHolderName}bank account linked to</Text>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.account_text}>XXXX5678</Text>
                            <Text style={styles.date_text}>15 APR 2019</Text>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => Actions.PaymentFailed()}
                        style={styles.tochable} >
                        <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            colors={['#3E1EDD', '#5C48C5']}
                            style={styles.gradient}>
                            <Text style={{ color: '#fff' }}>PROCEED</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
                <Dialog
                    visible={this.state.dialogclose}
                    onTouchOutside={() => {
                        this.setState({ dialogclose: false });
                    }}>
                    <DialogContent style={{
                        borderRadius: Platform.OS === 'android' ? null : null,
                        width: width / 2 + 70,
                        height: width / 2 - 20,
                        paddingTop: 10,
                    }}>
                        <View>
                            <View style={styles.Dialog_View}>
                                <Text style={styles.tranasfr_text}>You are transfering   {"\n"}     $200 to XYZ</Text>
                            </View>
                            <View style={styles.tochable_view}>
                                <TouchableOpacity>
                                    <View>
                                        <Text style={{ color: '#000' }}>Confirm</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <View>
                                        <Text style={{ color: '#000' }}>Cancel</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </DialogContent>
                </Dialog>
            </View>


        );
    }
}


export default PaymentSucess;
