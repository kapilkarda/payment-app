import React from "react";
import { View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity } from "react-native";
import { Actions } from 'react-native-router-flux';
import styles from './styles';
import AppIntroSlider from 'react-native-app-intro-slider';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height; //full heigh

const slides = [
    {
        image: require('../../components/icon/Group5362.png')
    },
    {
        image: require("../../components/icon/Group5363.png")
    },
    {
        image: require('../../components/icon/Group5364.png')
    },
];

class AppIntro extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            
        };
    };
    _renderItem = props => (
        <View style={{
            flex: 1, justifyContent: 'center', alignItems: "center", height: '100%', width: '100%', backgroundColor: '#fff'}}>
            {
                props.status == true &&
                <View style={{ height: "15%" }} />
            }
            <View style={{ alignItems: 'center', width: width }}>
                <Image style={{ height: '80%', width: '80%' }} source={props.item.image} />
            </View>
            <View style={{ position: 'absolute', top: 70, right: 60,  }}>
                <TouchableOpacity onPress={() => Actions.SignIn()}>
                <Text style={{fontSize: 15,color:'#391EBB',fontWeight:'bold'}}>SKIP</Text>
                </TouchableOpacity>
            </View>
        </View>
    );

    

    render() {
        return (
            <AppIntroSlider
                slides={slides}
                renderItem={this._renderItem}
                bottomButton
                nextLabel="Get Started"
                // showSkipButton
                doneLabel="Get Started"
                buttonTextStyle={{ color: "#fff", fontWeight: 'bold', }}
                activeDotStyle={{ backgroundColor: '#391EBB', }}
                buttonStyle={{
                    width: width - 60,
                    //height: (width * 0.12),
                    borderRadius: 30,
                    alignItems: 'center', alignSelf: 'center', marginBottom: 20,
                    backgroundColor: '#391EBB'
                }}

                onDone={() => Actions.SignIn()}
            //onSkip={() => console.log("skipped")}
            />
        );
    }
}


export default AppIntro;