import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, BackHandler, ToastAndroid,
} from "react-native";

import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import AsyncStorage from '@react-native-community/async-storage';
import styles from "./styles";

class RequestMoney extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            dialogclose: false, name: '', phone_number: '', amount: '',
        };
    };
    async componentWillMount() {
        const user_id = await AsyncStorage.getItem('user_id')
        const Contact = this.props.Contact
        console.log(Contact, "contact")
        this.setState({
            name: Contact.name,
            phone_number: Contact.phoneNumbers[0].number,
            user_id: user_id
        })

    }

    handleBackButtonClick = () => {
        this.setState({ dialogclose: false })
        this.setState({ dialogclose1: false })
        return true;
    }

    dialogopen() {
        this.setState({ dialogclose: true })
    }

    dialogclosebutton() {
        this.setState({ dialogclose: false })
        this.setState({ dialogclose1: false })
        // Actions.Login()
    }


    RequestValidation() {
        if (this.state.amount == "" || this.state.amount == null || this.state.amount == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter Amount'); },
                android: () => { ToastAndroid.show('Please enter Amount', ToastAndroid.SHORT); }
            })();
            return false;
        }
        Actions.RequstSucessfull({ Amount: this.state.amount, Name: this.state.name, phone_number: this.state.phone_number, })
        this.setState({ dialogclose: false })
    }



    render() {
        console.log(this.state.name, "name")
        return (
            <View style={{ flex: 1, backgroundColor: "#F3F3F3", }}>
                <AppHeader
                    headerTitle={'Request Money'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                    color={"#636363"}
                />

                <View style={styles.MainContainer}>
                    <View style={{ paddingHorizontal: 20, marginTop: 20 }}>
                        <View>
                            <Text style={styles.text_name}>{this.state.name}</Text>
                            <Text style={styles.text_name2}>Requesting Money from {this.state.name}</Text>
                        </View>
                        <View style={styles.view_input}>
                            <TextInput style={{
                                fontSize: RFValue(25)
                            }}
                                placeholder='$ Amount'
                                placeholderTextColor='#CECECE80'
                                underlineColorAndroid="transparent"
                                keyboardType="numeric"
                                returnKeyType='done'
                                onChangeText={(text) => this.setState({ amount: text })}
                                value={this.state.amount}>
                            </TextInput>
                        </View>
                    </View>
                </View>

                <TouchableOpacity style={styles.tochable}
                    onPress={() => this.dialogopen()}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={['#3E1EDD', '#5C48C5']}
                        style={styles.gradient}>
                        <Text style={styles.tochable_text}>REQUEST</Text>
                    </LinearGradient>
                </TouchableOpacity>
                <Dialog
                    visible={this.state.dialogclose}
                    // rounded={false}
                    onTouchOutside={() => {
                        this.setState({ dialogclose: false });
                    }} >
                    <DialogContent style={{
                        borderRadius: Platform.OS === 'android' ? null : null,
                        width: width - 80,
                        height: width / 2 - 20,
                        paddingTop: 10,
                    }}>
                        <View style={{ justifyContent: 'center' }}>
                            <View style={styles.View_text}>
                                <Text style={styles.req_text}>You are requesting</Text>
                                <Text style={styles.req_text}> $ {this.state.amount} to {this.state.name}</Text>
                            </View>
                            <View style={styles.dialog_btn}>
                                <TouchableOpacity onPress={() => this.dialogclosebutton()}>
                                    <View>
                                        <Text style={{ color: '#000' }}>CANCEL</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.RequestValidation()}>
                                    <View>
                                        <Text style={{ color: '#000' }}>CONFIRM</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </DialogContent>
                </Dialog>


            </View>


        );
    }
}


export default RequestMoney;
