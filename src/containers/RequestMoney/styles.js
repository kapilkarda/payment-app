import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    Main_View: {
        backgroundColor: '#fff',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        marginTop: 20, height: height,
        flex: 1
    },
    text_name: {
        color: '#000',
        fontWeight: '500',
        fontSize: RFValue(20),
        fontWeight: '900'
    },
    text_name2: {
        color: 'grey',
        fontSize: RFValue(15),
        fontWeight: '300'
    },
    view_input: {
        marginTop: 30,
        borderBottomColor: '#f3f3f3',
        borderBottomWidth: 1
    },
    tochable: {
        flex: 1,
        position: 'absolute',
        bottom: 40,
        alignSelf: 'center'
    },
    gradient: {
        height: 50,
        width: width,
        backgroundColor: '#391EBb',
        alignItems: 'center',
        justifyContent: 'center',
        width: width - 70,
        alignSelf: 'center',
        borderRadius: 30,
    },
    tochable_text: {
        color: '#fff',
        fontSize: RFValue(15),
        fontWeight: 'bold'
    },
    View_text: {
        alignItems: 'center',
        marginTop: 10,
        marginTop: 30
    },
    req_text: {
        color: '#351EBB',
        fontSize: RFValue(18),
        marginLeft: 10
    },
    dialog_btn: {
        alignSelf: 'flex-end',
        flexDirection: 'row',
        marginTop: 50, justifyContent: 'space-between',
        width: width / 3 + 20
    }


});
