import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput, ToastAndroid,
    ScrollView, CheckBox, Platform, KeyboardAvoidingView, Alert, BackHandler
} from "react-native";
import { Actions } from 'react-native-router-flux';
import styles from './styles';
import { heightPercentageToDP } from "react-native-responsive-screen";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from '../../components/Loader';
var checkpassword = ''
class RegisterNumber extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            checked: '',
            name: '', password: '', phone_number: '',
            isLoading: false

        };
    }
    // componentWillMount(){
    //     BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
    // }
    handlePressLogin = () => {
        this.Register()
    }
    Register() {

        const name = /^[A-Z a-z]+$/;
        if (name.test(this.state.name) === false) {
            Alert.alert("'Please enter Correct name'")
            return false;
        }
        const phone_number = /^[0]?[789]\d{9}$/;
        if (phone_number.test(this.state.phone_number) === false) {
            Alert.alert("'Please enter valid contact'")
            return false;
        }

        if (this.state.password.length == 6 || this.state.password.length >= 6) {
            checkpassword = 'Atleast 6 characters long.'
        } else {
            Alert.alert("'Password should 6 digits long'")
            return false;
        }

        this.RegisterNumber()
    }
    RegisterNumber = () => {
        this.setState({ isLoading: true })
        fetch('http://jokingfriend.com/Dero/index.php/Api/register', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "name": this.state.name,
                "phone_number": this.state.phone_number,
                "password": this.state.password,
                "device_type": "android",
                "device_token": "dhfcdsfbdfsefss"
            })
        })
            .then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === true || res.result === "true") {
                    // AsyncStorage.setItem('user_id', res.data.user_id)
                    this.setState({
                        isLoading: false
                    })
                    Actions.LoginPassword()
                }
                else {
                    alert(res.msg)
                    this.setState({
                        isLoading: false
                    })
                }

            })
            .catch((e) => {
                console.log('Error BeauticianListAPI')
                console.warn(e);
            });
    };
    render() {
        var padding
        Platform.select({
            ios: () => {
                padding = 'padding'
            },
            android: () => {
                padding = 0
            }
        })();

        return (
            <KeyboardAvoidingView
                behavior={padding}
                enabled style={{ flex: 1 }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ backgroundColor: "#fff", }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} >
                            <View style={{ marginLeft: 10, marginTop: 20 }}>
                                <Image style={{ height: 25, width: 25, tintColor: '#3F20DD' }}
                                    source={require('./../../components/icon/left-arrow.png')} />
                            </View>
                        </TouchableOpacity>
                        <View style={{ marginTop: '65%' }}>
                            <View style={{ alignSelf: 'center' }}>
                                <Text style={styles.Reg_text}>Register with Phone number</Text>
                            </View>

                            <View style={styles.View1}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={{ height: 20, width: 20, }}
                                        source={require('../../components/icon/login3.png')} />
                                </View>
                                <View style={styles.ViewInput}>
                                    <TextInput placeholder="Name"
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor="grey"
                                        returnKeyType='next'
                                        underlineColorAndroid='transparent'
                                        onChangeText={(text) => this.setState({ name: text })}
                                        value={this.state.name}>
                                    </TextInput>
                                </View>
                            </View>
                            <View style={styles.View1}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={{ height: 20, width: 20, }}
                                        source={require('../../components/icon/reg7.png')} />
                                </View>
                                <View style={[styles.ViewInput, { marginLeft: 6 }]}>
                                    <TextInput placeholder="Phone Number"
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor="grey"
                                        underlineColorAndroid='transparent'
                                        maxLength={10}
                                        returnKeyType='next'
                                        keyboardType="numeric"
                                        onChangeText={(text) => this.setState({ phone_number: text })}
                                        value={this.state.phone_number}>

                                    </TextInput>
                                </View>
                            </View>
                            <View style={styles.View1}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={{ height: 20, width: 15, }}
                                        source={require('../../components/icon/pass5.png')} />
                                </View>
                                <View style={styles.ViewInput}>
                                    <TextInput placeholder="Pasword"
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor="grey"
                                        secureTextEntry={true}
                                        returnKeyType='done'
                                        underlineColorAndroid='transparent'
                                        onChangeText={(text) => this.setState({ password: text })}
                                        value={this.state.password}
                                        onSubmitEditing={this.handlePressLogin}>
                                    </TextInput>
                                </View>

                            </View>
                            <TouchableOpacity onPress={() => this.Register()}>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    colors={['#3E1EDD', '#5C48C5']}
                                    style={styles.gradient}>
                                    <Text style={{ color: '#fff', fontWeight: 'bold' }}>Register</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            <View style={{ height: 20 }}></View>
                        </View>
                    </View>
                    {this.state.isLoading &&
                        <Spinner />
                    }
                </ScrollView>

            </KeyboardAvoidingView>
        );
    }
}


export default RegisterNumber;
