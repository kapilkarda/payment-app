import { StyleSheet,Dimensions } from 'react-native';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default StyleSheet.create({
    View1: {
        flexDirection: 'row',
        width: width - 50,
        alignSelf: 'center',
        borderColor: '#391Ebb',
        borderWidth: 1,
        height: 50,
        borderRadius: 25,
        paddingHorizontal: 20,
        marginTop: 20
    },
    ViewInput:{
        justifyContent: 'center',
        marginLeft: 10,
         width: width / 2 + 50
    },
    Reg_text:{
        color: '#391Ebb', fontSize: RFValue(25), fontWeight: '500'
       },
       gradient:{
        flexDirection: 'row', width: width - 50, alignSelf: 'center', backgroundColor: '#391Ebb',
        height: 50, borderRadius: 25, alignItems: 'center', justifyContent: 'center', marginTop: 30
       }
});
