import React from "react";
import { View, Text, StatusBar, Image, Dimensions, ImageBackground,
     TouchableOpacity ,BackHandler
} from "react-native";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
 import LinearGradient from 'react-native-linear-gradient';
class SignIn extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: ''
        };
    };
    // componentWillMount(){
    //     BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
    // }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#fff" }}>
                <View style={{ flex: 1, position: 'absolute', bottom: 150, alignSelf: 'center' }}>
                    <TouchableOpacity onPress={() => Actions.RegisterNumber()}>
                   {/* < LinearGradient colors={['#3E1EDD','#5C48C5']} */}
                   <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#3E1EDD','#5C48C5']} style={{
                            backgroundColor: '#391EBB', width: width - 50, height: 50,
                            alignSelf: 'center', alignItems: 'center', justifyContent: 'center', borderRadius: 25,
                        }}>
                            <Text style={{ color: '#fff', fontSize: RFValue(15), fontWeight: 'bold' }}>Register with Phone Number</Text>

                       </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => Actions.LoginBankId()}>
                    {/* < LinearGradient colors={['#3E1EDD','#5C48C5']} */}
                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#3E1EDD','#5C48C5']} style={{
                            backgroundColor: '#391EBB', width: width - 50, height: 50,
                            alignSelf: 'center', alignItems: 'center', justifyContent: 'center', borderRadius: 25, marginTop: 15
                        }}>
                            <Text style={{ color: '#fff', fontSize: RFValue(15), fontWeight: 'bold' }}>Login with BankID</Text>

                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => Actions.LoginPassword()}>
                    {/* < LinearGradient colors={['#3E1EDD','#5C48C5']}  */}
                   <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#3E1EDD','#5C48C5']} style={{
                            backgroundColor: '#391EBB', width: width - 50, height: 50,
                            alignSelf: 'center', alignItems: 'center', justifyContent: 'center', borderRadius: 25, marginTop: 15
                        }}>
                            <Text style={{ color: '#fff', fontSize: RFValue(15), fontWeight: 'bold' }}>Login with Password</Text>

                      </LinearGradient>
                    </TouchableOpacity>
                    {/* <LinearGradient colors={['#3E1EDD','#5C48C5']} style={{
                        flex: 1,
                        paddingLeft: 15,
                        paddingRight: 15,
                        borderRadius: 5,marginTop:10
                    }}>
                        <Text style={{
                            fontSize: 18,
                            fontFamily: 'Gill Sans',
                            textAlign: 'center',
                            margin: 10,
                            color: '#ffffff',
                            backgroundColor: 'transparent',
                        }}>
                            Sign in with Facebook
  </Text>
                    </LinearGradient> */}

                </View>


            </View>
        );
    }
}


export default SignIn;
