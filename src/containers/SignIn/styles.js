import { StyleSheet } from 'react-native';
// import { Colors } from '../../Theme';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export const COLOR = {
DARK: "#040207",
PANTOME: '#ff6f61',
LIGHT: "#ffffff",
BLACK: "#000",
GRAY: "#9A9A9A",
LIGHT_GRAY: "#ffffff",
DANGER: "#FF5370",
RED: "#800000",
WHITE:"#FFF",
CYAN: "#09818F",
LIGHT_ORANGE:"#ff944d"
};

export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
        alignItems: 'center',
        justifyContent: 'center',
      },
      QR_text: {
        color: '#000',
        fontSize: 19,
        padding: 8,
        marginTop: 12
      },
      button: {
        backgroundColor: '#2979FF',
        alignItems: 'center',
        padding: 12,
        width: 300,
        marginTop: 14
      },
});