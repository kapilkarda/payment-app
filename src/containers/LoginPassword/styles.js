import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#fff",
    },
    View1: {
        flexDirection: 'row',
        width: width - 50,
        alignSelf: 'center',
        borderColor: '#391Ebb',
        borderWidth: 1,
        height: 50,
        borderRadius: 25,
        paddingHorizontal:
            20, marginTop: 20
    },
    ViewInput: {
        justifyContent: 'center',
        width: width / 2 + 50,
        marginLeft: 10
    },
    CheckView: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 60
    },
    CheckText: {
        color: '#000',
        fontSize: RFValue(12)
    },
    gradient: {
        flexDirection: 'row',
        width: width - 50,
        alignSelf: 'center',
        backgroundColor: '#391Ebb',
        height: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10
    },
    text: {
        color: '#391Ebb',
        fontSize: RFValue(25),
        fontWeight: '500'
    }


});
