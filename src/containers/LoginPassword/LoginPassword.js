import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput, Alert, ToastAndroid,
    ScrollView, BackHandler
} from "react-native";
import { Actions } from 'react-native-router-flux';
import styles from './styles';
import { heightPercentageToDP } from "react-native-responsive-screen";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import CheckBox from 'react-native-check-box'
import Spinner from '../../components/Loader';
import LinearGradient from 'react-native-linear-gradient';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AsyncStorage from '@react-native-community/async-storage';
var checkpassword = ''
class LoginPassword extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            checked: '', phone_number: '', password: '',
            isChecked: false, isLoading: false

        };
    }
    // componentWillMount(){
    //     BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
    // }
    handlePressLogin = () => {
        this.Login()
    }
    Login() {
        const regp = /^[0]?[789]\d{9}$/;
        if (regp.test(this.state.phone_number) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter valid contact'); },
                android: () => { ToastAndroid.show('Please enter valid contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
        
        if (this.state.password.length == 6 || this.state.password.length >= 6) {
            checkpassword = 'Atleast 6 characters long.'
        } else {
            Alert.alert("'Password should 6 digits long'")
            return false;
        }
        this.termaccept()

    }
    termaccept() {
        if (this.state.isChecked) {
            this.LoginPassword()
            this.setState({
                isChecked: true
            });
            console.log('panc', this.state.isChecked)
        } else {
            Platform.select({
                ios: () => { AlertIOS.alert('Please accept terms and conditions.'); },
                android: () => { ToastAndroid.show('Please accept terms and conditions.', ToastAndroid.SHORT); }
            })();
        }
    }
    LoginPassword = () => {
        this.setState({ isLoading: true })
        fetch('http://jokingfriend.com/Dero/index.php/Api/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "phone_number": this.state.phone_number,
                "password": this.state.password,
                "device_type": "android",
                "device_token": "rsgvesgrsgvs"
            })
        })
            .then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === true || res.result === "true") {
                    AsyncStorage.setItem('user_id', res.data.user_id)
                    AsyncStorage.setItem('user_name', res.data.name)
                    AsyncStorage.setItem('phone_number',res.data.phone_number)
                    AsyncStorage.setItem('Acess_token',res.token)

                    this.setState({
                        isLoading: false
                    })
                    Actions.tabbar()
                }
                else {
                    alert(res.msg)
                    console.log(res.msg)
                    this.setState({
                        isLoading: false
                    })
                }

            })
            .catch((e) => {
                console.log('Error BeauticianListAPI')
                console.warn(e);

            });
    };
    render() {
        return (
            <View style={styles.MainContainer}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} >
                    <View style={{ marginLeft: 10, marginTop: 20 }}>
                        <Image style={{ height: 25, width: 25, tintColor: '#3F20DD' }}
                            source={require('./../../components/icon/left-arrow.png')} ></Image>
                    </View>
                </TouchableOpacity>
                <ScrollView>
                    <View style={{ marginTop: '60%' }}>

                        <View style={{ alignSelf: 'center' }}>
                            <Text style={styles.text}>Login with Password</Text>
                        </View>

                        <View style={styles.View1}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={{ height: 20, width: 20, }}
                                    source={require('../../components/icon/login3.png')} />
                            </View>
                            <View style={styles.ViewInput}>
                                <TextInput placeholder="Mobile Number"
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor="grey"
                                    keyboardType="numeric"
                                    maxLength={10}
                                    returnKeyType='next'
                                    onChangeText={(text) => this.setState({ phone_number: text })}
                                    value={this.state.phone_number} />
                            </View>
                        </View>
                        <View style={styles.View1}>
                            <View style={{ justifyContent: 'center' }}>
                                <Image style={{ height: 20, width: 15, }}
                                    source={require('../../components/icon/pass5.png')} />
                            </View>
                            <View style={[styles.ViewInput, { marginLeft: 12 }]}>
                                <TextInput placeholder="Pasword"
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor="grey"
                                    secureTextEntry={true}
                                    returnKeyType='done'
                                    onChangeText={(text) => this.setState({ password: text })}
                                    value={this.state.password}
                                    onSubmitEditing={this.handlePressLogin} />
                            </View>
                        </View>

                        <View style={styles.CheckView}>
                            <CheckBox
                                onClick={() => {
                                    this.setState({
                                        isChecked: !this.state.isChecked
                                    })
                                }}
                                isChecked={this.state.isChecked}
                                checkedCheckBoxColor={'#391EBB'} />

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={styles.CheckText}>  I, agree to the </Text>
                                <TouchableOpacity>
                                    <Text style={[styles.CheckText, { textDecorationLine: 'underline' }]}>terms of service</Text>
                                </TouchableOpacity>
                                <Text style={styles.CheckText}> and </Text>
                                <TouchableOpacity>
                                    <Text style={[styles.CheckText, { textDecorationLine: 'underline' }]}>privacy policy</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                        <TouchableOpacity onPress={() => this.Login()}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={['#3E1EDD', '#5C48C5']} style={styles.gradient}>
                                <Text style={{ color: '#fff', fontWeight: 'bold' }}>Login</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                        <View style={{ height: 100 }} />
                    </View>
                </ScrollView>
                {this.state.isLoading &&
                    <Spinner />
                }
            </View>
        );
    }
}


export default LoginPassword;
