import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    view_text: {
        paddingHorizontal: 20,
        marginTop: 20,
        height: 200
    },
    text_name: {
        color: '#000',
        fontWeight: '500',
        fontSize: RFValue(20)
    },
    account_name: {
        color: 'grey',
        fontSize: RFValue(15),
        fontWeight: '300',
        marginTop: 10
    },
    input_View: {
        marginTop: 30,
        width: width - 50,
        borderBottomColor: '#f3f3f3',
        borderBottomWidth: 1
    },
    MyBank_View: {
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        flex: 4
    },

    view_list: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 5
    },
    text: {
        fontSize: RFValue(15),
        color: '#000'
    },
    list_View: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        minWidth: width / 3 + 20
    }






});
