import React from "react";
import {
    View,
    Image,
    Dimensions,
    TextInput,
    Text,
    TouchableOpacity,
    FlatList,
    BackHandler,
    ScrollView, KeyboardAvoidingView, Platform, ToastAndroid, AlertIOS, alert
} from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import { Footer, FooterTab, Button, Header, Radio, radioStatus } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
// import { CheckBox } from 'react-native-elements'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
//import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox';
import RadioButton from 'react-native-radio-button'
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles'


const data = [{
    img: require('./../../components/icon/addbank1.png'),
    img2: require("./../../components/icon/radio.png"),
    name: 'Swedbank',
    account: 'xxxxxx45677',
    checked: true
},

{
    img: require('./../../components/icon/addbank1.png'),
    img2: require("./../../components/icon/radio1.png"),
    name: 'Swedbank',
    account: 'xxxxxx45678',
    checked: false
},
{
    img: require('./../../components/icon/addbank1.png'),
    img2: require("./../../components/icon/radio1.png"),
    name: 'Swedbank',
    account: 'xxxxxx45679',
    checked: false
},
{
    img: require('./../../components/icon/addbank1.png'),
    img2: require("./../../components/icon/radio1.png"),
    name: 'Swedbank',
    account: 'xxxxxx45680',
    checked: false
}]

class PaymentSend extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            amount: '',
            valuess: 0,
            value1: 0,
            dialogclose: false,
            radioStatus: false,
            notChecked: true,
            checked: false,
            array: [],
            showList: true,
            Amount: '', phone_number: '', name: '', user_id: '',
        };
    };
    async componentWillMount() {
        this.setState({
            array: data
        })
        const user_id = await AsyncStorage.getItem('user_id')
        const Contact = this.props.Contact
        console.log(Contact, "contact")
        this.setState({
            Name: Contact.displayName,
            phone_number: Contact.phoneNumbers[0].number,
            user_id: user_id
        })
        console.log(this.state.Name, "name")
    }
    AmountValidation() {
        if (this.state.amount == "" || this.state.amount == null || this.state.amount == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter Amount'); },
                android: () => { ToastAndroid.show('Please enter Amount', ToastAndroid.SHORT); }
            })();
            return false;
        }
        this.setState({ dialogclose: true })
    }
    handleBackButtonClick = () => {
        this.setState({ dialogclose: false })
        this.setState({ dialogclose1: false })
        return true;
    }
    dialogopen() {
        this.setState({ dialogclose: true })
    }
    dialogclosebutton() {
        this.setState({ dialogclose: false })
        this.setState({ dialogclose1: false })
        // Actions.Login()
    }
    fun() {
        Actions.PaymentSucess({ AccountHolderName: this.state.Name })
        this.setState({ dialogclose: false })
    }
    render() {
        var padding
        Platform.select({
            ios: () => {
                padding = "padding"
            },
            android: () => {
                padding = ""
            }
        })();
        toggleRadio = () => {
            const { radioStatus } = this.state;
            this.setState({ radioStatus: !radioStatus })
        };
        doSomething = (name) => {
            console.log(name)
            if (name == 0) {
                this.setState({ checked: true })
            }
        }
        return (
            <View style={{ flex: 1, backgroundColor: "#FFF", }}>
                <View style={{ flex: 1 }}>
                    <View>
                        <AppHeader
                            headerTitle={'Send Money'}
                            backgo={require('./../../components/icon/left-arrow.png')}
                            onPress={() => this.props.navigation.goBack()}
                            color={"#636363"}
                        />
                    </View>
                    <View style={styles.view_text}>
                        <View>
                            <Text style={styles.text_name}>{this.state.name}</Text>
                            <Text style={styles.account_name}>This {this.state.Name} is linked to bank a/c XXXX3456</Text>
                        </View>
                        <View style={styles.input_View}>
                            <TextInput style={{ fontSize: RFValue(25) }}
                                placeholder='$ Amount'
                                placeholderTextColor='#CECECE80'
                                keyboardType="numeric"
                                onChangeText={(text) => this.setState({ amount: text })}
                                value={this.state.amount}>
                            </TextInput>
                        </View>
                    </View>

                    {/* </KeyboardAvoidingView> */}
                    <View style={styles.MyBank_View}>
                        <View style={{ padding: 20 }}>
                            <Text style={{ fontSize: RFValue(15), color: '#000' }}>My Bank</Text>
                        </View>
                        <ScrollView>
                            {this.state.showList == true &&
                                <View>
                                    {
                                        this.state.array && this.state.showList == true && this.state.array.map((item, i) =>
                                            <View key={i}>
                                                <View style={styles.view_list}>
                                                    <View style={styles.list_View}>
                                                        <View >
                                                            <Image style={{ height: 50, width: 50 }} source={item.img} />
                                                        </View>
                                                        <View style={{ alignSelf: 'center', }}>
                                                            <Text style={styles.text}>{item.name}</Text>
                                                            <Text style={styles.text}>{item.account}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ justifyContent: 'center' }}>
                                                        <RadioButton
                                                            animation={'bounceIn'}
                                                            isSelected={item.checked}
                                                            onPress={() => doSomething(i)}
                                                            outerColor={'#391EBB'}
                                                            innerColor={'#391EBB'}
                                                            size={10}
                                                        />
                                                    </View>


                                                </View>
                                            </View>
                                        )
                                    }
                                </View>
                            }
                        </ScrollView>
                    </View>
                </View>

                <View style={{ backgroundColor: '#fff', padding: 10, alignItems: 'center', width: width }} >
                    <TouchableOpacity onPress={() => this.AmountValidation()} >
                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#3E1EDD', '#5C48C5']} style={{ backgroundColor: '#391EBB', width: width - 50, height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30 }}>
                            <Text style={{ color: '#fff', fontSize: RFValue(20) }}>PAY</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                </View>
                <Dialog
                    visible={this.state.dialogclose}
                    onTouchOutside={() => {
                        this.setState({ dialogclose: false });
                    }} >
                    <DialogContent style={{
                        borderRadius: Platform.OS === 'android' ? null : null,
                        width: width - 80,
                        height: width / 2 - 20,
                        paddingTop: 10,
                    }}>
                        <View style={{ justifyContent: 'center' }}>
                            <View style={{ alignItems: 'center', marginTop: 10, marginTop: 30 }}>
                                <Text style={{ color: '#351EBB', fontSize: RFValue(18), }}>You are transfering</Text>
                                <Text style={{ color: '#351EBB', fontSize: RFValue(18), }}> $200 to {this.state.Name}</Text>
                            </View>
                            <View style={{ alignSelf: 'flex-end', flexDirection: 'row', marginTop: 50, justifyContent: 'space-between', width: width / 3 + 20 }}>
                                <TouchableOpacity onPress={() => this.dialogclosebutton()}>
                                    <View>
                                        <Text style={{ color: '#000' }}>CANCEL</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.fun()}>
                                    <View>
                                        <Text style={{ color: '#000' }}>CONFIRM</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </DialogContent>
                </Dialog>
            </View>



        );
    }
}


export default PaymentSend;
