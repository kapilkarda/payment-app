import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, BackHandler
} from "react-native";
import styles from './styles';
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
class AddAccount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: ''
        };
    };

    render() {
        return (
            <View style={styles.MainContainer}>
                <AppHeader
                    headerTitle={'Add Bank Account'}
                    backgo={require('./../../components/icon/left-arrow.png')}
                    onPress={() => this.props.navigation.goBack()}
                    // tintColor={'#3F20DD'}
                    color={"#000"} />

                <View style={styles.View1}>
                    <View style={{ alignItems: 'center', marginTop: '5%' }}>
                        <Image style={{ height: 50, width: 50 }}
                            source={require("../../components/icon/addbank1.png")} />
                        <Text style={[styles.text, { marginTop: 5 }]}>Nordea</Text>
                    </View>
                    <View style={{ marginTop: 10, paddingHorizontal: 25, }}>
                        <Text style={styles.text}>Add Details</Text>
                    </View>
                    <View style={{ paddingHorizontal: 25, marginTop: 15 }}>
                        <View style={styles.inputView}>
                            <TextInput style={{ marginLeft: 10 }} placeholder='Account Holder Name'
                                placeholderTextColor='#66666680' />
                        </View>
                        <View style={[styles.inputView, { marginTop: 20 }]}>
                            <TextInput style={{ marginLeft: 10 }} placeholder='Account Number'
                                placeholderTextColor='#66666680' />
                        </View>
                        <View style={[styles.inputView, { marginTop: 20 }]}>
                            <TextInput style={{ marginLeft: 10 }} placeholder='IFSC Code'
                                placeholderTextColor='#66666680' />
                        </View>
                    </View>
                </View>
                <TouchableOpacity style={{ marginTop: '7%', paddingHorizontal: 25 }}>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#3E1EDD', '#5C48C5']}
                        style={styles.gradient}>
                        <Text style={styles.TochableText}>ADD A ACCOUNT</Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );
    }
}
export default AddAccount;
