import { StyleSheet, Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default StyleSheet.create({
 MainContainer: {
        flex: 1,
        backgroundColor: "#F3F3F3",
    },
    View1: {
        backgroundColor: '#fff',
        marginTop: 10,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        height: width + 30
    },
    inputView: {
        borderColor: 'grey',
        borderWidth: 0.6,
        height: 40,
        justifyContent: 'center',
        borderRadius: 5
    },
    gradient: {
        backgroundColor: '#5B45C5',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
    },
    text:{
        color: '#000', 
        fontSize: RFValue(15),
    },
    TochableText:{
        color: '#fff', 
        fontWeight: 'bold', 
        fontSize: RFValue(15)
    }
});