import { StyleSheet } from 'react-native';


export const COLOR = {
  DARK: "#040207",
  PANTOME: '#ff6f61',
  LIGHT: "#ffffff",
  BLACK: "#000",
  GRAY: "#9A9A9A",
  LIGHT_GRAY: "#ffffff",
  DANGER: "#FF5370",
  RED: "#800000",
  WHITE: "#FFF",
  CYAN: "#09818F",
  YELLOW: "#cccc00"
};

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:'white'
      },
      button: {
        alignItems: 'center',
        backgroundColor: '#2c3539',
        padding: 10,
        width:300,
        marginTop:16
      },
      heading: { 
        color: 'black', 
        fontSize: 24, 
        alignSelf: 'center', 
        padding: 10, 
        marginTop: 30 
      },
      simpleText: { 
        color: 'black', 
        fontSize: 20, 
        alignSelf: 'center', 
        padding: 10, 
        marginTop: 16
      }
});
