import React from "react";
import {
    View, Image, Dimensions,
    TextInput, Text, TouchableOpacity, ScrollView, FlatList, PermissionsAndroid,TouchableHighlight,BackHandler
} from "react-native";
import styles from './styles'
import { Footer, FooterTab, Button, Header } from "native-base";
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import AppHeader from '../../components/AppHeader';
import { CameraKitCameraScreen, CAMERA } from 'react-native-camera-kit';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

class ScannerReader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        user_id: '',
        QR_Code_Value: '',
        Start_Scanner: false,
    };
};
componentWillMount() {
    this.open_QR_Code_Scanner()
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
}
openLink_in_browser = () => {

    Linking.openURL(this.state.QR_Code_Value);

}
onQR_Code_Scan_Done = (QR_Code) => {

    this.setState({ QR_Code_Value: QR_Code });
    alert(QR_Code)
    Actions.PaymentSend({QRvalue:this.state.QR_Code_Value})
    console.log(this.state.QR_Code_Value)
    this.setState({ Start_Scanner: false });
}
open_QR_Code_Scanner = () => {

    var that = this;

    if (Platform.OS === 'android') {
        async function requestCameraPermission() {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA, {
                        'title': 'Camera App Permission',
                        'message': 'Camera App needs access to your camera '
                    }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {

                    that.setState({ QR_Code_Value: '' });
                    that.setState({ Start_Scanner: true });
                } else {
                    alert("CAMERA permission denied");
                }
            } catch (err) {
                alert("Camera permission err", err);
                console.warn(err);
            }
        }
        requestCameraPermission();
    } else {
        that.setState({ QR_Code_Value: '' });
        that.setState({ Start_Scanner: true });
    }
}

render() {

    return (
        <View style={{ flex: 1, backgroundColor: "#F3F3F3", }}>
            {/* <AppHeader
                headerTitle={'Request Money'}
                backgo={require("../../components/Images/back.png")}
                onPress={() => this.props.navigation.goBack()}
            /> */}
            <Header hasTabs
                style={{ backgroundColor: "#fff", justifyContent: 'center', }}  >
                <View style={{ width: width, justifyContent: 'center', }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                        style={{ marginLeft: 10, position: 'absolute', left: 0, }}>
                        <Image source={require('./../../components/icon/left-arrow.png')}
                            style={{ width: 25, height: 25, tintColor: '#3F20DD' }} />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ color: "grey", fontWeight: 'bold', alignSelf: 'center' }}>
                            Send Money</Text>
                    </View>
                </View>
            </Header>
            
            <View style={{ flex:1}}>
              
        <CameraKitCameraScreen
       
          showFrame={true}
          //Show/hide scan frame
          
          scanBarcode={true}
          //Can restrict for the QR Code only
          laserColor={'blue'}
          //Color can be of your choice
          frameColor={'transparent'}
          //If frame is visible then frame color
          colorForScannerFrame={'black'}
        //   flashImages={{
        //     // Flash button images
        //     on: require('./assets/flashon.png'),
        //     off: require('./assets/flashoff.png'),
        //     auto: require('./assets/flashauto.png'),
        //   }}
          //Scanner Frame color
          onReadCode={event =>
            this.onQR_Code_Scan_Done(event.nativeEvent.codeStringValue)
          }
        />
      </View>

        </View>


    );
}
}

export default ScannerReader;
