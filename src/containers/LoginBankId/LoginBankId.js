import React from "react";
import {
    View, Text, StatusBar, Image, Dimensions, ImageBackground, TouchableOpacity, TextInput,
    ScrollView, Platform, KeyboardAvoidingView,BackHandler
} from "react-native";
import { Actions } from 'react-native-router-flux';
import styles from './styles';
import { heightPercentageToDP } from "react-native-responsive-screen";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import CheckBox from 'react-native-check-box'

import LinearGradient from 'react-native-linear-gradient';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class LoginBankId extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: '',
        };
    }
    // componentWillMount(){
    //     BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
    // }
    render() {
        var padding
        Platform.select({
            ios: () => {
                padding = 'padding'
            },
            android: () => {
                padding = 0
            }
        })();
        
        return (
            <KeyboardAvoidingView
                behavior={padding}
                enabled style={{ flex: 1 }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={styles.MainContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} >
                            <View style={{ marginLeft: 10, marginTop: 20 }}>
                                <Image style={styles.arrowImage}
                                    source={require('./../../components/icon/left-arrow.png')} />
                            </View>
                        </TouchableOpacity>
                        <View style={{ marginTop: '40%' }}>
                            <View style={{ alignSelf: 'center' }}>
                                <Text style={{ color: '#391Ebb', fontSize: RFValue(25), fontWeight: '500' }}>Login with Bank Id</Text>
                            </View>
                            <View style={styles.View1}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={{ height: 20, width: 20, }}
                                     source={require('../../components/icon/login3.png')} />
                                </View>
                                <View style={styles.Viewinput}>
                                    <TextInput placeholder="Name"
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor="grey">
                                    </TextInput>
                                </View>
                            </View>
                            <View style={styles.View1}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={{ height: 20, width: 20, tintColor: 'gray' }}
                                        source={require('../../components/icon/login2.png')} />
                                </View>
                                <View style={styles.Viewinput}>
                                    <TextInput placeholder="Account Number"
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor="grey">
                                    </TextInput>
                                </View>
                            </View>
                            <View style={styles.View1}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={{ height: 20, width: 15, }}
                                        source={require('../../components/icon/pass5.png')} />
                                </View>
                                <View style={[styles.Viewinput, { marginLeft: 12 }]}>
                                    <TextInput placeholder="Pasword"
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor="grey">
                                    </TextInput>
                                </View>
                            </View>
                            <View style={styles.CheckView}>
                                <CheckBox
                                    onClick={() => {
                                        this.setState({
                                            isChecked: !this.state.isChecked
                                        })
                                    }}
                                    isChecked={this.state.isChecked}
                                    checkedCheckBoxColor={'#391EBB'} />
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={styles.CheckText}>  I, agree to the </Text>
                                    <TouchableOpacity>
                                        <Text style={[styles.CheckText, { textDecorationLine: 'underline' }]}>terms of service</Text>
                                    </TouchableOpacity>
                                    <Text style={styles.CheckText}> and </Text>
                                    <TouchableOpacity>
                                        <Text style={[styles.CheckText, { textDecorationLine: 'underline' }]}>privacy policy</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <TouchableOpacity 
                            // onPress={() => Actions.Home()}
                            >
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    colors={['#3E1EDD', '#5C48C5']}
                                    style={styles.gradient}>
                                    <Text style={{ color: '#fff', fontWeight: 'bold' }}>Login</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            <View style={{ height: 50 }}></View>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}
export default LoginBankId;
