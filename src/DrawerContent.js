import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, ViewPropTypes,TouchableOpacity, Image, Dimensions,} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    paddingHorizontal:5
  },
});

class DrawerContent extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    sceneStyle: ViewPropTypes.style,
    title: PropTypes.string,
  };

  static contextTypes = {
    drawer: PropTypes.object,
  };

  render() {
    return (
      <View style={styles.container}>
          <TouchableOpacity onPress={this.toggleOpen} style={styles.animatedBox}>
                <View style={{flex:1}}>
                    <View style={{ alignSelf: 'center', marginTop: 20 }}>
                        <TouchableOpacity
                         onPress={() => Actions.Profile()}
                        >
                        <Image source={require("./../src/components/Images/Profile1.png")}
                            style={{ width: wp('38%'), height: hp("25%"), marginTop: 3 }}>
                        </Image>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                       onPress={() => Actions.Homepage()}
                    >
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            alignSelf: 'center',
                            justifyContent: 'center',
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:20,
                            paddingHorizontal:13
                        }}>
                            <View style={{flexDirection:'row'}}>
                                <Image source={require("./../src/components/Images/icon.png")}
                                    style={{ width: wp('3.5%'), height: hp('2.5%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Home</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                    >
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            alignSelf: 'center',
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:6,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("./../src/components/Images/notification.png")}
                                    style={{ width: wp('3.2%'), height: hp('2.5%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Notifications</Text>
                            </View>
                            <View style={{flexDirection:'row',marginLeft:10,alignSelf:'center'}}>
                                <Image source={require("./../src/components/Images/notify.png")}
                                    style={{ width: wp('8%'), height: hp('4%'),}}>
                                </Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                    >
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            alignSelf: 'center',
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:6,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("./../src/components/Images/cart.png")}
                                    style={{ width: wp('4.5%'), height: hp('2%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Cart</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                    >
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            alignSelf: 'center',
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:6,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("./../src/components/Images/orders.png")}
                                    style={{ width: wp('3.2%'), height: hp('2.5%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Orders</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity 
                    >
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            alignSelf: 'center',
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:6,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("./../src/components/Images/setting.png")}
                                    style={{ width: wp('4%'), height: hp('2%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Settings</Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity style={{
                        position:'absolute',
                        bottom:90,
                        alignSelf: 'center',
                    }}>
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:10,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13,
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("./../src/components/Images/signupdrawer.png")}
                                    style={{ width: wp('5%'), height: hp('2%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Sign up as a vendor</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        position:'absolute',
                        bottom:40,
                        alignSelf: 'center',
                    }}>
                        <View style={{
                            height: hp('6%'),
                            width: wp('60%'),
                            backgroundColor: '#5dca67',
                            borderRadius:20,
                            marginTop:10,
                            flexDirection:'row',
                            justifyContent:'space-between',
                            paddingHorizontal:13,
                        }}>
                            <View style={{flexDirection:'row',alignSelf:'center'}}>
                                <Image source={require("./../src/components/Images/logout.png")}
                                    style={{ width: wp('5%'), height: hp('2.5%'),tintColor:'#fff',}}>
                                </Image>
                                <Text style={{color:'#fff',fontWeight:'bold',fontSize:10,marginLeft:10}}>Log Out</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>

      </View>
    );
  }
}

export default DrawerContent;